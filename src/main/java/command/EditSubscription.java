package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 *  Edits order 
 * @author Nikita Trufanov
 *
 */
public class EditSubscription extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		HttpSession session = request.getSession();
		if (session.getAttribute("position") == null || !session.getAttribute("position").equals(2)) {
			throw new WebException("You don`t have a permission to enter here");
		}
		String status = request.getParameter("status");
		String subsId = request.getParameter("subs_id");
		String bookId = request.getParameter("book_id");
		if (status == null || status.isBlank() || subsId == null || subsId.isBlank() || !subsId.matches("[-+]?\\d+") || bookId == null || bookId.isBlank() || !bookId.matches("[-+]?\\d+")) {
			throw new WebException("Wrong inputs");
		}
		int id = Integer.parseInt(subsId);
		int bookIdInt = Integer.parseInt(bookId);
		if (status.equals("APROVED") || status.equals("DECLINED") || status.equals("ENDED")) {
			if (db.checkSubStatus(id, status)) {
				db.editSubStatus(id, status, bookIdInt);
				page = PathContainer.SUCCESS;
			} else {
				throw new WebException("Cant modify order with chosen status");
			}
		} else {
			throw new WebException("Wrong status modifier");
		}
		return page;
	}

}
