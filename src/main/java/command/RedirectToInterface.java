package command;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Redirects to interface
 * @author Nikita Trufanov
 *
 */
public class RedirectToInterface extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		HttpSession session = request.getSession();
		Object positionTemp = session.getAttribute("position");
		if(positionTemp == null || !positionTemp.toString().matches("[-+]?\\d+")){
			throw new WebException("Wrong position");
		}
		int position = (int)positionTemp;
		String email = (String) session.getAttribute("email");
		if (email == null || email.isBlank()) {
			throw new WebException("Wrong inputs");
		}
		if (db.userBanned(email) == 0) {

			switch (position) {
			case 1:
				session.setAttribute("position_name", "User");
				page = PathContainer.USER;
				break;
			case 2:
				session.setAttribute("position_name", "Librarian");
				page = PathContainer.LIBRARIAN;
				break;
			case 3:
				session.setAttribute("position_name", "Admin");
				page = PathContainer.ADMIN;
				break;
			default:
				page = PathContainer.DEFAULT;
			}

		} else {

			throw new WebException("You are banned");
		}
		return page;

	}
}
