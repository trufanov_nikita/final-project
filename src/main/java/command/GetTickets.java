package command;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.Subscription;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for all orders with pagination for librarian
 * @author Nikita Trufanov
 *
 */
public class GetTickets extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		HttpSession session = request.getSession();
		if(session.getAttribute("position") == null || !session.getAttribute("position").equals(2)) {
			throw new WebException("You don`t have a permission to enter here");
		}
		DBManager db = DBManager.createDb();
		int number = db.findNumberOfTickets();
		String currentPageString =(request.getParameter("page"));
		if(currentPageString == null || currentPageString.isBlank() || !currentPageString.matches("[-+]?\\d+")) {
			throw new WebException("Wrong inputs");
		}
		int currentPage = Integer.parseInt(currentPageString);
        int recordsPerPage = 6;
        int numberOfPages = (int)Math.ceil(number*1.0
                / recordsPerPage);
        if (request.getParameter("page") != null)
        	currentPage = Integer.parseInt(
                request.getParameter("page"));
        List<Subscription> list = db.findLimitTickets(
        		(currentPage - 1) * recordsPerPage, recordsPerPage);
        request.setAttribute("list_subs", list);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", currentPage);
        page = PathContainer.LIBRARIAN_TICKETS;
		
		return page;
	}

}
