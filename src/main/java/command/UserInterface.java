package command;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Redirects to user interface
 * @author Nikita Trufanov
 *
 */
public class UserInterface extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		HttpSession session = request.getSession();
		String page;
		if(session.getAttribute("position").equals(1)) {
			page = PathContainer.USER;
		}
		else {
			throw new WebException("You don`t have a permission to enter here");
			
		}
		return page;
	}
}
