package command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Author;
import classes.Book;
import database.DBManager;
import exception.WebException;
import controller.PathContainer;

/**
 * 
 * @author Nikita Trufanov
 * Adds book to database
 *
 */
public class AddBook extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
    	String page;
		HttpSession session = request.getSession();
		DBManager db = DBManager.createDb();
		response.setContentType("text/html"); 
		if(session.getAttribute("position") == null) {
			throw new WebException("You are not loged in");
		}
		else if(!session.getAttribute("position").equals(3)) {
			throw new WebException("You dont have permission for this operation");
		}
		String author_name = request.getParameter("book_author_name");
		String author_surname = request.getParameter("book_author_surname");
		String book_name = request.getParameter("book_name");
		String book_number = request.getParameter("book_number");
		if(author_name == null || author_surname == null || book_name == null|| book_number == null) {
			throw new WebException("Some information is null");
		}
		if(author_name.isBlank() || author_surname.isBlank() || book_name.isBlank() || book_number.isBlank()) {
			throw new WebException("Some information is blank");
		}
		if(!book_number.matches("[-+]?\\d+")) {
			throw new WebException("Wrong number format");
		}
		int book_number_int =Integer.parseInt(book_number);
		Author author = Author.createAuthor(author_name, author_surname);
		if(db.findAuthorId(author)== 0) {
			db.addAuthor(author);
		}
		author = db.findAuthorByNames(author);
				
		if(!db.checkBook(Book.createBook(book_name, author, book_number_int))) {
			Book book = Book.createBook(book_name, author, book_number_int);
			
			db.addBook(book);
			
			page=PathContainer.SUCCESS;
				
		}
		else {
			throw new WebException("We already have this book");
		}
			return page;
    	}
    }

