package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Adds librarian to database
 *
 */
public class AddLibrarian extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;	
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		if(session.getAttribute("position") == null) {
			throw new WebException("You are not loged in");
		}
		else if(!session.getAttribute("position").equals(3)) {
			throw new WebException("You dont have permission for this operation");
		}
		DBManager db = DBManager.createDb();
		String email=request.getParameter("librarian_input");
		if(email == null) {
			throw new WebException("Email cant be null");
		}
		User user = db.findUser(email);
		if(user == null) {
			throw new WebException("We dont find this user");
		}
		db.createLibrarian(email);
		page = PathContainer.SUCCESS;
		return page;
	}


}
