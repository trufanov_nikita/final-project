package command;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for books with pagination for user interface
 * @author Nikita Trufanov
 *
 */
public class GetCatalog extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		int number = db.findNumberOfBooks();
        int recordsPerPage = 10;
        String currentPageString =(request.getParameter("page"));
		if(currentPageString == null || currentPageString.isBlank() || !currentPageString.matches("[-+]?\\d+")) {
			throw new WebException("Wrong inputs");
		}
		int currentPage = Integer.parseInt(currentPageString);
		int numberOfPages = (int)Math.ceil(number*1.0
                / recordsPerPage);
        List<Book> list = db.findLimitBooks(
        		(currentPage - 1) * recordsPerPage, recordsPerPage);
        request.setAttribute("bookList", list);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", currentPage);
        page = PathContainer.CATALOG;
		
		return page;
	}

}
