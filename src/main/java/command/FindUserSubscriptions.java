package command;

import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Subscription;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for user orders
 * @author Nikita Trufanov
 *
 */
public class FindUserSubscriptions extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		response.setCharacterEncoding("UTF-8");
		DBManager db = DBManager.createDb();
		String email = request.getParameter("search_input");
		if(email != null) {
			User user = db.findUser(email);
			if(user == null) {
				throw new WebException("No such user");
			}
			response.setContentType("text/html"); 
			List<Subscription> subs= db.findUserSubs(user.getId());
			request.setAttribute("list_subs", subs);
			page = PathContainer.FIND_USER_SUBS;
		}
		else {
			throw new WebException("You are not logged in");
		}
		return page;
	}


}
