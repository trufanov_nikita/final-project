package command;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Unbans user
 * @author Nikita Trufanov
 *
 */
public class UnBan extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		DBManager db = DBManager.createDb();
		String page;
		HttpSession session = request.getSession();
		if (session.getAttribute("position") != null && session.getAttribute("position").equals(3)) {
			db = DBManager.createDb();
			String email = request.getParameter("unban_input");
			if (email == null || email.isBlank()) {
				throw new WebException("incorrect email(null or blank)");
			}
			if (db.findUser(email) != null) {
				db.unbanUser(email);
				page = PathContainer.SUCCESS;
			} else {
				throw new WebException("No such user");
			}
			return page;
		} else {
			throw new WebException("You don`t have a permission to enter here");
		}
	}

}
