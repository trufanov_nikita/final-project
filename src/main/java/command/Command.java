package command;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exception.WebException;

public abstract class Command implements Serializable {

	private static final long serialVersionUID = 7389929608544179692L;

	public abstract String execute(HttpServletRequest request,
			HttpServletResponse response) throws WebException;

}
