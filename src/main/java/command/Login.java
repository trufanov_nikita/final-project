package command;

import java.util.List;

import javax.servlet.http.*;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Login command. Checks correct email and password, redirects to correspanding interface
 * @author Nikita Trufanov
 *
 */
public class Login extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
			
		DBManager db = DBManager.createDb();
		HttpSession session = request.getSession();
		String pass=request.getParameter("password");
		String email=request.getParameter("email");
		if(pass == null || email == null || pass.isBlank() || email.isBlank()) {
			throw new WebException("Wrong inputs");
		}
		if(db.isPassCorrect(email, pass) ){
			User user = db.findUser(email);
			int pos = user.getPosition();
			session.setAttribute("user", user);
			session.setAttribute("name", user.getName());
			session.setAttribute("surname", user.getSurname());
			session.setAttribute("email", email);
			session.setAttribute("position", pos);
			if(db.userBanned(email)==0){
				
				switch (pos) {
				case 1: 
					session.setAttribute("position_name", "User");
					page = PathContainer.USER;
					break;
				case 2: 
					session.setAttribute("position_name", "Librarian");
					page = PathContainer.LIBRARIAN;
					break;
				case 3: 
					session.setAttribute("position_name", "Admin");
					page = PathContainer.ADMIN;
					break;
				default :
					page=PathContainer.DEFAULT;
				}
				
			}
			else {
				throw new WebException("You are banned. Try to contact with librarian or admin");
			}
			
		}
		else{
			throw new WebException("Wrong password");
		}
		return page;
		
	}

}
