package command;


import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

/**
 * Searches for most popular books
 */
public class MostPopular extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
			response.setContentType("text/html"); 
		DBManager db=DBManager.createDb();
		List<Book>popularBooks=db.findMostPopularBooks();
		List<Book> books_first = new ArrayList<>();
		List<Book> books_second = new ArrayList<>();
		for(int i=0; i<popularBooks.size(); i++) {
			if(i%2==0) {
				books_first.add(popularBooks.get(i));
			}else {
				books_second.add(popularBooks.get(i));
			}
		}
			
		request.setAttribute("list_books_first", books_first);
		request.setAttribute("list_books_second", books_second);
		request.setAttribute("list_books", popularBooks);
		return PathContainer.BOOKS;
	}

}
