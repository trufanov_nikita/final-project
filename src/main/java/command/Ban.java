package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Bans user
 *
 */
public class Ban extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		HttpSession session = request.getSession();
		String page;
		if (session.getAttribute("position") != null && session.getAttribute("position").equals(3)) {

			DBManager db = DBManager.createDb();
			String email = request.getParameter("ban_input");
			if (email == null || email.isBlank()) {
				throw new WebException("incorrect email(null or blank)");
			}
			if (db.findUser(email) != null) {
				db.banUser(email);
				page = PathContainer.SUCCESS;
			} else {
				throw new WebException("No such user");
			}
			return page;
		} else {
			throw new WebException("You don`t have a permission to enter here");
		}
	}

}
