package command;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for users with pagination for admin
 * @author Nikita Trufanov
 *
 */
public class GetUsers extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
			String page;
			HttpSession session = request.getSession();
			if(session.getAttribute("position") == null || !session.getAttribute("position").equals(3)) {
				throw new WebException("You don`t have a permission to enter here");
			}
			DBManager db = DBManager.createDb();
			int number = db.findNumberOfUsers();
			String currentPageString =(request.getParameter("page"));
			if(currentPageString == null || currentPageString.isBlank() || !currentPageString.matches("[-+]?\\d+")) {
				throw new WebException("Wrong inputs");
			}
			int currentPage = Integer.parseInt(currentPageString);
	        int recordsPerPage = 10;
	        int numberOfPages = (int)Math.ceil(number*1.0
	                / recordsPerPage);
	        List<User> list = db.findLimitUsers(
	        		(currentPage - 1) * recordsPerPage, recordsPerPage);
	        request.setAttribute("list_users", list);
	        request.setAttribute("numberOfPages", numberOfPages);
	        request.setAttribute("currentPage", currentPage);
	        page = PathContainer.ADMIN_USERS;
			
			return page;
	}

}
