package command;

import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Subscription;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Shows subscriptions of current user
 * @author Nikita Trufanov
 *
 */
public class UserSubscriptions extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		HttpSession session = request.getSession();
		DBManager db = DBManager.createDb();
		String email = (String) session.getAttribute("email");
		if(email != null) {
			User user = db.findUser(email);
			List<Subscription> subs= db.findUserSubs(user.getId());
			request.setAttribute("list_subs", subs);
			page = PathContainer.SUBSCRIPTIONS;
		}
		else {
			throw new WebException("Wrong Email");
		}
		return page;
	}


}
