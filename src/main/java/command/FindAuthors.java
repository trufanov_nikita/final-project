package command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for Authors
 * @author John
 *
 */
public class FindAuthors extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		
		final List<Book> books = db.findBooksWithAuthors();
		Map <Author, List<Book>> authors= new HashMap<>();
		books.forEach(x -> {
			Author author = x.getAuthor();
			if(!authors.containsKey(author)) {
				int authorId = author.getId();
				List<Book> auhtorBooks = books.stream().filter(i -> i.getAuthor().getId() == authorId).collect(Collectors.toList());
				authors.put(author, auhtorBooks);
			}
			
		});
		request.setAttribute("authors_map", authors);
		page = PathContainer.AUTHORS;
		
		return page;
	}




}