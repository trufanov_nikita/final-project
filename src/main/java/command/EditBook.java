package command;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Edits a book
 *
 */
public class EditBook extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		HttpSession session = request.getSession();
		if (session.getAttribute("position") == null || !session.getAttribute("position").equals(3)) {
			throw new WebException("You don`t have a permission to enter here");
		}
		String book_id = request.getParameter("book_id");
		String book_number = request.getParameter("book_number");
		String book_name = request.getParameter("book_name");
		String author_name = request.getParameter("book_author_name");
		String author_surname = request.getParameter("book_author_surname");
		int book_id_int;
		int book_number_int;
		if(verify(book_id, book_number, book_name, author_name, author_surname)) {
			throw new WebException("Wrong inputs");
		}
		else {
			book_id_int = Integer.parseInt(book_id);
			book_number_int = Integer.parseInt(book_number);
		}

		String author_name_change = request.getParameter("book_author_name_change");
		String author_surname_change = request.getParameter("book_author_surname_change");
		String book_name_change = request.getParameter("book_name_change");
		String book_number_change =request.getParameter("book_number_change");
		int book_number_change_int = 0;
		if(author_name_change == null || author_name_change.isBlank()) {
			author_name_change = author_name;
		}
		if(author_surname_change == null || author_surname_change.isBlank()) {
			author_surname_change = author_surname;
		}
		if(book_name_change == null || book_name_change.isBlank()) {
			book_name_change = book_name;
		}
		if(book_number_change == null || book_number_change.isBlank()) {
			book_number_change_int = book_number_int;
		}
		else {
			book_number_change_int = Integer.parseInt(book_number_change);
		}
		Author author = Author.createAuthor(author_name_change, author_surname_change);
		if (db.findAuthorId(author) == 0) {
			db.addAuthor(author);
		}
		author = db.findAuthorByNames(author);
		Book changeBook = Book.createBook(book_id_int, book_name_change,author, book_number_change_int);

		if(db.editBook(changeBook)) {
			page = PathContainer.ADMIN;
		}
		else {
			throw new WebException("Failed to edit a book");
		}
		return page;
	}

		private boolean verify(String book_id, String book_number, String book_name, String author_name,
				String author_surname) {
			return book_id == null || book_number == null|| book_name == null|| author_name == null|| author_surname == null || book_id.isBlank() || book_number.isBlank()|| book_name.isBlank()|| author_name.isBlank()|| author_surname.isBlank() || !book_number.matches("[-+]?\\d+") || !book_id.matches("[-+]?\\d+");
		}

	

}
