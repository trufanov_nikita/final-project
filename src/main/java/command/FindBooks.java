package command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for books
 * @author Nikita Trufanov
 *
 */
public class FindBooks extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		DBManager db = DBManager.createDb();
		List<Book> books = new ArrayList<Book>();
		List<Book> books_first = new ArrayList<Book>();
		List<Book> books_second = new ArrayList<Book>();
		books = db.findLimitBooks(0, 10);
			for(int i=0; i<books.size(); i++) {
				if(i%2==0) {
					books_first.add(books.get(i));
				}else {
					books_second.add(books.get(i));
				}
			}
			request.setAttribute("list_books_first", books_first);
			request.setAttribute("list_books_second", books_second);
			page = PathContainer.BOOKS;
			return page;
	}

}
