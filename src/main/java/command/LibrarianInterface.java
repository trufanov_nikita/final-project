package command;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Redirects to librarian interface if you are librarian
 * @author Nikita Trufanov
 *
 */
public class LibrarianInterface extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		HttpSession session = request.getSession();
		String page;
		if(session.getAttribute("position")!= null && session.getAttribute("position").equals(2)) {
			page = PathContainer.LIBRARIAN;
		}
		else {
			throw new WebException("You don`t have a permission to enter here");
			
		}
		return page;
	}
}
