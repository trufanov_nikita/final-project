package command;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

/**
 * Registers user
 * @author Nikita Trufanov
 *
 */
public class Register extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		HttpSession session = request.getSession();
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String language = request.getParameter("language");
		DBManager db = DBManager.createDb();
		if(db.isEmailBusy(email)) {
			throw new WebException("Sorry this email is busy");
		}
		else{
			
			db.addUser(name, surname, email, password);
			User user = db.findUser(email);
			session.setAttribute("user", User.createUser(name, surname, email, 1));
			session.setAttribute("name", name);
			session.setAttribute("email", email);
			session.setAttribute("position", 1);
			page = PathContainer.USER;
			
		}
		return page;	
		
	}

}
