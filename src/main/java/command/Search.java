package command;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * Searches for book
 * @author Nikita Trufanov
 *
 */
public class Search extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		String radioInput = request.getParameter("radio_input");
		DBManager db = DBManager.createDb();
		String search = request.getParameter("search_input");
		
		if (radioInput == null || search == null || search.isBlank()) {
			throw new WebException("Wrong inputs");
		}
		search = search.trim();
		List<Book> books;
		switch(radioInput) {
		case ("book"):
			books = db.findSearchBook(search);
			break;
		case ("author"):
			books = db.findSearchByAuthor(search);
			break;
		default:
			books = db.findSearchBook(search);
			break;
		}
		if (books.isEmpty()) {
			throw new WebException("Nothing was found by your search");
		}
				request.setAttribute("search", "Your search is " + search);
				request.setAttribute("list_books", books);
				page = PathContainer.SEARCH;


		return page;
	}
}
