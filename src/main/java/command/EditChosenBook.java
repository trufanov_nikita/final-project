package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Redirects to Edit book interface
 *
 */
public class EditChosenBook extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

		@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		String page;
		HttpSession session = request.getSession();
		if (session.getAttribute("position") == null || !session.getAttribute("position").equals(3)) {
			throw new WebException("You don`t have a permission to enter here");
		}
		String authorName = request.getParameter("book_author_name");
		String authorSurname = request.getParameter("book_author_surname");
		String bookName = request.getParameter("book_name");
		String bookNumber = request.getParameter("book_number");
		String bookId = request.getParameter("book_id");
		int bookNumberInt;
		int bookNumberId;
		if(checkData(bookId, bookNumber, bookName, authorName, authorSurname)) {
			throw new WebException("Wronk inputs");
		}
		else {
			bookNumberInt = Integer.parseInt(bookNumber);
			bookNumberId = Integer.parseInt(bookId);
		}
		Book book = Book.createBook(bookNumberId, bookName, Author.createAuthor(authorName, authorSurname), bookNumberInt);
		request.setAttribute("book", book);
		page = PathContainer.EDIT;
		return page;
	}
		private boolean checkData(String book_id, String book_number, String book_name, String author_name,
				String author_surname) {
			return book_id == null || book_number == null || book_name == null || author_name == null || author_surname == null || 
					book_id.isBlank() || book_number.isBlank() || book_name.isBlank() || author_name.isBlank() || author_surname.isBlank() || !book_number.matches("[-+]?\\d+") || !book_id.matches("[-+]?\\d+");
		}

}
