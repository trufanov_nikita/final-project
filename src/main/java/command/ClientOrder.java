package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.Author;
import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Adds order 
 *
 */
public class ClientOrder extends Command {

	private static final long serialVersionUID = -8898169921347685006L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws WebException {
		HttpSession session = request.getSession();
		DBManager db = DBManager.createDb();
		String page;
		if (session.getAttribute("position") == null) {
			throw new WebException("You are not loged in");
		}
		User user = db.findUser((String) session.getAttribute("email"));
		if (user == null) {
			throw new WebException("wrong email, try to login again");
		}
		response.setContentType("text/html");
		String bookId = request.getParameter("book_id");
		String bookNumber = request.getParameter("book_number");
		String bookName = request.getParameter("book_name");
		String authorName = request.getParameter("book_author_name");
		String authorSurname = request.getParameter("book_author_surname");

		if (checkData(bookId, bookNumber, bookName, authorName, authorSurname)) {
			throw new WebException("Wronk inputs");
		}
		int bookIdInt = Integer.parseInt(bookId);
		int bookNumberInt = Integer.parseInt(bookNumber);
		Author author = Author.createAuthor(authorName, authorSurname);
		Book book = Book.createBook(bookIdInt, bookName, author, bookNumberInt);
		if (book.getBookNumber() < 1) {
			throw new WebException("We dont have this book now. Try later");
		}
		if (db.addTicket(book, user.getId())) {
			page = PathContainer.SUCCESS;
		} else {
			throw new WebException("Cant order this book");
		}

		return page;
	}

	private boolean checkData(String bookId, String bookNumber, String bookName, String authorName,
			String authorSurname) {
		return bookId == null || bookNumber == null || bookName == null || authorName == null
				|| authorSurname == null || bookId.isBlank() || bookNumber.isBlank() || bookName.isBlank()
				|| authorName.isBlank() || authorSurname.isBlank() || !bookNumber.matches("[-+]?\\d+")
				|| !bookId.matches("[-+]?\\d+");
	}

}
