package classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import database.DBManager;
import exception.WebException;

/**
 * @author Nikita Trufanov
 * Represents an author
 * 
 */
public class Author {

	private String name;
	private String surname;
	private List<Book> books = new ArrayList<>();
	int id;

	public Author(String name, String surname) {
		this.name = name;
		this.surname = surname;

	}

	public Author(int id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
	}

	public Author(int id, String name, String surname, List<Book> books) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void booksAdd(Book book) {
		this.books.add(book);
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public int getId() {
		return id;
	}

	public String toString() {
		return "ID" + id + " Author[Name " + name + " Surname: " + surname + " Books: " + books.toString() + "]";
	}

	public static Author createAuthor(String name, String surname) {
		return new Author(name, surname);
	}

	public static Author createAuthor(int id, String name, String surname) {
		return new Author(id, name, surname);
	}

	public static Author createAuthor(int id, String name, String surname, List<Book> books) {
		return new Author(id, name, surname, books);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, surname);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		return id == other.id && Objects.equals(name, other.name) && Objects.equals(surname, other.surname);
	}
}
