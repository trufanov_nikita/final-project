package classes;

import java.util.Objects;

/**
 * @author Nikita Trufanov
 * Represents a book
 */
public class Book {
	private String name;
	private Author author;
	/**
	 * number of orders placed for this book
	 */
	private int visits;
	private int bookNumber;
	private int id;

	public Author getAuthor() {
		return author;
	}

	public String getName() {
		return name;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public int getVisits() {
		return visits;
	}

	public void setVisits(int visits) {
		this.visits = visits;
	}

	public int getId() {
		return id;
	}

	public int getBookNumber() {
		return bookNumber;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Book(int id, String name, Author author) {
		this.name = name;
		this.author = author;
		this.id = id;
	}

	public Book(String name, Author author) {
		this.name = name;
		this.author = author;

	}

	public Book(String name, int visits, Author author) {
		this.name = name;
		this.author = author;
		this.visits = visits;
	}

	public Book(String name, Author author, int bookNumber) {
		this.name = name;
		this.author = author;
		this.bookNumber = bookNumber;
	}

	public Book(String name, int bookNumber) {
		this.name = name;
		this.bookNumber = bookNumber;
	}

	public Book(String name, int visits, Author author, int bookNumber) {
		this.name = name;
		this.author = author;
		this.visits = visits;
		this.bookNumber = bookNumber;
	}

	public Book(int id, String name, Author author, int bookNumber) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.bookNumber = bookNumber;
	}

	public Book(int id, String name, Author author, int bookNumber, int visits) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.visits = visits;
		this.bookNumber = bookNumber;
	}

	public String toString() {
		return "Book " + getId() + " name: " + getName() + " Number of books" + getBookNumber() + " " + this.getVisits()
				+ " " + this.getAuthor() + "\n";
	}

	public static Book createBook(String name, Author author, int bookNumber) {
		return new Book(name, author, bookNumber);
	}

	public static Book createBook(String name, int bookNumber) {
		return new Book(name, bookNumber);
	}

	public static Book createBook(String name, int visits, Author author, int bookNumber) {
		return new Book(name, visits, author, bookNumber);
	}

	public static Book createBook(int id, String name, Author author, int bookNumber) {
		return new Book(id, name, author, bookNumber);
	}

	public static Book createBook(int id, String name, Author author, int bookNumber, int visits) {
		return new Book(id, name, author, bookNumber, visits);
	}

	@Override
	public int hashCode() {
		return Objects.hash(author, bookNumber, id, name, visits);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return author.equals(other.author) && bookNumber == other.bookNumber && id == other.id
				&& Objects.equals(name, other.name) && visits == other.visits;
	}
}
