package classes;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
/**
 * 
 * @author Nikita Trufanov
 * Represents a Subscription
 */
public class Subscription {
	private int id;
	private Book book;
	private User user;
	private int fee;
	private String date;
	private String status;
	private Subscription(int id, Book book, User user, String status,String formattedDate, int fee) {
		this.book = book;
		this.user = user;
		this.id = id;
		this.status = status;
		this.date = formattedDate;
		this.fee = fee;
		
	}
	public int getId() {
		return id;
	}
	public Book getBook() {
		return book;
	}
	public User getUser() {
		return user;
	}
	public int getFee() {
		return fee;
	}
	public String getDate() {
		return date;
	}
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param id Id of subscription in database
	 * @param user User witch ordered a book
	 * @param book Book that has been ordered
	 * @param status Current state of order
	 * @param date Deadline of return
	 * @param fee  Amount of money needed to pay for book(only if deadline has expired and book wasn`t returned)
	 * @return Subscription
	 */
	public static Subscription createSubscription(int id, User user, Book book, String status, LocalDateTime date, int fee) {
	    DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy MM HH:mm:ss/E, MMM dd");

		String formattedDate = date.format(formater); 
		return new Subscription(id, book, user, status, formattedDate, fee);
	}
	
}
