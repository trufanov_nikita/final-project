package classes;

import java.util.Objects;
/**
 * 
 * @author Nikita Trufanov
 * represents a User
 */
public class User {

	private String name;
	private String surname;
	private String email;
	private String password;
	private int id;
	private String ban;
	private int position;
	
	public String getBan() {
		return ban;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public int getPosition() {
		return position;
	}

	public User(String name, String surname, String email, int position) {
		this.name = name;
		this.email = email;
		this.surname = surname;
		this.position = position;
	}

	public User(int id, String name, String surname, String email, int position, String isBanned) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.surname = surname;
		this.position = position;
		this.ban = isBanned;
	}

	public static User createUser(String name, String surname, String email, int position) {
		return new User(name, surname, email, position);
	}
/**
 * 
 * @param id
 * @param name
 * @param surname
 * @param email
 * @param position position in library
 * @param isBanned represents is user banned or not
 * @return User
 */
	public static User createUser(int id, String name, String surname, String email, int position, String isBanned) {
		return new User(id, name, surname, email, position, isBanned);
	}

	public String toString() {
		return "Name: " + name + " Surname: " + surname + " Email: " + email + ban;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ban, email, id, name, password, position, surname);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(ban, other.ban) && Objects.equals(email, other.email) && id == other.id
				&& Objects.equals(name, other.name) && Objects.equals(password, other.password)
				&& position == other.position && Objects.equals(surname, other.surname);
	}

}
