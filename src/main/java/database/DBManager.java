package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import classes.Author;
import classes.Book;
import classes.Subscription;
import classes.User;
import exception.WebException;
/**
 * 
 * @author Nikita Trufanov
 * Represents DBManager witch controlles all operations with database
 */
public class DBManager {
	private static final String CONNECTION_URL = "jdbc:mysql://127.0.0.1:3306/mydb?user=root&password=baluha";
	private static final Logger LOG = Logger.getLogger(DBManager.class);
	private static DBManager instance;
/**
 * 
 * @return new DBManager if wasn`t created or created DBManager  
 */
	public static DBManager createDb() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}
/**
 * 
 * @return Connection
 * @throws WebException
 */
	public Connection getConnection() throws WebException {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(CONNECTION_URL);
			con.setAutoCommit(false);
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Cannot obtain connection", e);
			throw new WebException("Cannot obtain connection", e);
		}
		return con;
	}
	/**
	 * 
	 * @param email
	 * @return true if email is already existed or false in opposite 
	 * @throws WebException
	 */
	public boolean isEmailBusy(String email) throws WebException {
		String query = "Select * from users where email = ?";
		ResultSet rs = null;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			rs = stmt.executeQuery();
			if (rs.next()) {
				return true;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return false;
	}
	/**
	 * 
	 * @param email
	 * @param pass
	 * @return true if database has email with this password, false if opposite
	 * @throws WebException
	 */
	public boolean isPassCorrect(String email, String pass) throws WebException {
		String query = "Select * from users where password=? and email=?;";
		Connection con = getConnection();

		ResultSet rs = null;
		boolean res = false;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, pass);
			stmt.setString(2, email);
			rs = stmt.executeQuery();
			while (rs.next()) {
				res = true;
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return res;
	}
	/**
	 * Adds author to database
	 * @param author
	 * @throws WebException
	 */
	public void addAuthor(Author author) throws WebException {
		String query = "Insert INTO authors (name, surname)  Values (?,?); ";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, author.getName());
			stmt.setString(2, author.getSurname());
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
	}
	/**
	 * Adds user to database
	 * @param name
	 * @param surname
	 * @param email
	 * @param password
	 * @throws WebException
	 */
	public void addUser(String name, String surname, String email, String password) throws WebException {
		String query = "Insert INTO users (name, surname, email, password, position_id, isBanned)  Values (?,?,?,?,1,0)";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, name);
			stmt.setString(2, surname);
			stmt.setString(3, email);
			stmt.setString(4, password);
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
	}
	/**
	 * 
	 * @param author
	 * @return id of author
	 * @throws WebException
	 */
	public int findAuthorId(Author author) throws WebException {
		String query = "SELECT * FROM authors where name=? and surname=?;";
		int res = 0;
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, author.getName());
			stmt.setString(2, author.getSurname());
			rs = stmt.executeQuery();
			while (rs.next()) {
				res = rs.getInt(1);
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return res;
	}
	/**
	 * Searches for all books of selected author
	 * @param author
	 * @return List of books
	 * @throws WebException
	 */
	public List<Book> findAuthorsBooks(Author author) throws WebException {
		List<Book> books = new ArrayList<>();
		String query = "SELECT * FROM books where authors_id=?;";
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, author.getId());
			rs = stmt.executeQuery();
			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2), author, rs.getInt(5)));
			}
			con.commit();

		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}

		return books;
	}
	/**
	 * Searches Author with selected id
	 * @param id
	 * @return Author
	 * @throws WebException
	 */
	public Author findAuthorById(int id) throws WebException {
		String query = "SELECT * FROM mydb.authors where id=?;";
		Author author = null;
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			while (rs.next()) {
				author = Author.createAuthor(rs.getInt(1), rs.getString(2), rs.getString(3));
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}

		return author;
	}
	/**
	 * Searches author by name and surname
	 * @param author
	 * @return Author
	 * @throws WebException
	 */
	public Author findAuthorByNames(Author author) throws WebException {
		String query = "SELECT * FROM mydb.authors where name=? and surname=?;";
		Author authorRes = null;
		ResultSet rs = null;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, author.getName());
			stmt.setString(2, author.getSurname());
			rs = stmt.executeQuery();
			while (rs.next()) {
				authorRes = Author.createAuthor(rs.getInt(1), rs.getString(2), rs.getString(3));
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return authorRes;
	}
	/**
	 * Searches user by selected email
	 * @param email
	 * @return User
	 * @throws WebException
	 */
	public User findUser(String email) throws WebException {
		String query = "Select * from users where email = ?;";
		User user = null;
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String ban;
				if (rs.getInt(7) == 0) {
					ban = "No";
				} else {
					ban = "Yes";
				}
				user = User.createUser(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(6),
						ban);
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return user;
	}
	/**
	 * Searches user by selected id
	 * @param email
	 * @return User
	 * @throws WebException
	 */
	public User findUserById(int id) throws WebException {
		String query = "Select * from users where id = ?;";
		User user = null;
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String ban;
				if (rs.getInt(7) == 0) {
					ban = "No";
				} else {
					ban = "Yes";
				}
				user = User.createUser(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(6),
						ban);
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return user;
	}
	/**
	 * 
	 * @return number of books in database
	 * @throws WebException
	 */
	public int findNumberOfBooks() throws WebException {
		String query = "SELECT COUNT(*) FROM books;";
		int result = 0;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				result = rs.getInt(1);
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return result;

	}
	/**
	 * 
	 * @return number of users in database
	 * @throws WebException
	 */
	public int findNumberOfUsers() throws WebException {
		String query = "SELECT COUNT(*) FROM users;";
		int result = 0;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				result = rs.getInt(1);
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return result;

	}
	/**
	 * 
	 * @return number of orders in database
	 * @throws WebException
	 */
	public int findNumberOfTickets() throws WebException {
		String query = "SELECT COUNT(*) FROM ticket;";
		int result = 0;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				result = rs.getInt(1);
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return result;

	}
	/**
	 * 
	 * @return number of authors in database
	 * @throws WebException
	 */
	public int findNumberOfAuthors() throws WebException {
		String query = "SELECT COUNT(*) FROM authors;";
		int result = 0;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				result = rs.getInt(1);
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return result;

	}
	/**
	 * Searches for books with limit
	 * @param number start of limit
	 * @param step number of books we want to get
	 * @return List of books
	 * @throws WebException
	 */
	public List<Book> findLimitBooks(int number, int step) throws WebException {
		String query = "SELECT\r\n" + "	books.id as book_id, books.name as book_name, book_number as book_number,\r\n"
				+ "	authors.id as author_id, authors.name as author_name, authors.surname as author_surname\r\n"
				+ "FROM books\r\n" + "INNER JOIN authors ON authors.id=books.authors_id order by authors_id limit ?, ?";
		Connection con = getConnection();
		List<Book> books = new ArrayList<>();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, number);
			stmt.setInt(2, step);
			rs = stmt.executeQuery();
			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(4), rs.getString(5), rs.getString(6)), rs.getInt(3)));
			}
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}

		return books;
	}
	/**
	 * Searches for orders with limit
	 * @param number start of limit
	 * @param step number of orders we want to get
	 * @return List of orders
	 * @throws WebException
	 */
	public List<Subscription> findLimitTickets(int number, int step) throws WebException {
		List<Subscription> subs = new ArrayList<>();
		String query = "SELECT *\r\n" + "  FROM ticket\r\n" + "LEFT OUTER JOIN users\r\n"
				+ "  ON ticket.user_id = users.id\r\n" + "LEFT OUTER JOIN books\r\n"
				+ "  ON ticket.book_id = books.id\r\n" + "LEFT OUTER JOIN authors\r\n"
				+ "	ON books.authors_id = authors.id\r\n" + "    ORDER by status limit ?, ?";
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, number);
			stmt.setInt(2, step);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String ban;
				if (rs.getInt(13) == 0) {
					ban = "No";
				} else {
					ban = "Yes";
				}
				User user = User.createUser(rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10),
						rs.getInt(12), ban);
				Book book = Book.createBook(rs.getInt(14), rs.getString(15),
						Author.createAuthor(rs.getInt(19), rs.getString(20), rs.getString(21)), rs.getInt(18));
				if (rs.getString(4).equals("APROVED")) {
					LocalDateTime current = LocalDateTime.now();
					LocalDateTime time = LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime());
					if (time.isBefore(current) && rs.getInt(6) == 0) {
						addFee(rs.getInt(1), con);
					}
				}
				subs.add(Subscription.createSubscription(rs.getInt(1), user, book, rs.getString(4),
						LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime()), rs.getInt(6)));

			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return subs;
	}
	/**
	 * Searches for users with limit
	 * @param number start of limit
	 * @param step number of users we want to get
	 * @return List of users
	 * @throws WebException
	 */
	public List<User> findLimitUsers(int number, int step) throws WebException {
		List<User> users = new ArrayList<>();
		String query = "Select * from users ORDER by email limit ?, ? ";
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, number);
			stmt.setInt(2, step);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String ban;
				if (rs.getInt(7) == 0) {
					ban = "No";
				} else {
					ban = "Yes";
				}
				users.add(User.createUser(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(6),
						ban));

			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return users;
	}
	/**
	 * Searches for books witch name is like entered
	 * @param search Name of book we want to find
	 * @return List of possible books 
	 * @throws WebException
	 */
	public List<Book> findSearchBook(String search) throws WebException {
		List<Book> books = new ArrayList<>();
		String query = "SELECT books.id, books.name, book_number, visitors,	authors.id, authors.name, authors.surname FROM books INNER JOIN authors ON authors.id=books.authors_id where books.name like(?);";
		ResultSet rs = null;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, "%" + search + "%");
			rs = stmt.executeQuery();
			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(5), rs.getString(6), rs.getString(7)), rs.getInt(3), rs.getInt(4)));
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return books;
	}
	/**
	 * Searches for authors witch name or surname is like entered
	 * @param search Name or surname of author we want to find
	 * @return List of possible authors 
	 * @throws WebException
	 */
	public List<Book> findSearchByAuthor(String search) throws WebException {
		List<Book> books = new ArrayList<>();
		String query = "SELECT books.id, books.name, book_number, visitors,	authors.id, authors.name, authors.surname FROM books INNER JOIN authors ON authors.id=books.authors_id where authors.name like(?) or authors.surname like(?);";
		ResultSet rs = null;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, "%" + search + "%");
			stmt.setString(2, "%" + search + "%");
			rs = stmt.executeQuery();
			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(5), rs.getString(6), rs.getString(7)), rs.getInt(3), rs.getInt(4)));
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return books;
	}
	/**
	 * Returns first 10 books with most visits
	 * @return List of books
	 * @throws WebException
	 */
	public List<Book> findMostPopularBooks() throws WebException {
		List<Book> books = new ArrayList<>();
		String query = "SELECT\r\n" + "	books.id, books.name, book_number, visitors,\r\n"
				+ "	authors.id, authors.name, authors.surname\r\n"
				+ "FROM books \r\n" + "INNER JOIN authors ON authors.id=books.authors_id \r\n"
				+ "order by books.visitors desc limit 10;";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {

			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(5), rs.getString(6), rs.getString(7)), rs.getInt(3), rs.getInt(4)));
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return books;
	}
	/**
	 * Check is user banned
	 * @param email
	 * @return 0 if not
	 * @throws WebException
	 */
	public int userBanned(String email) throws WebException {
		String query = "Select isBanned from users where email=?;";
		int result = 0;
		Connection con = getConnection();
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			rs = stmt.executeQuery();

			while (rs.next()) {
				result = rs.getInt(1);
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return result;

	}
	/**
	 * Bans user with entered email
	 * @param email
	 * @throws WebException
	 */
	public void banUser(String email) throws WebException {
		String query = "UPDATE users Set isBanned=1 where email=? and position_id<3;";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

	}
	/**
	 * Unban user with entered email
	 * @param email
	 * @throws WebException
	 */
	public void unbanUser(String email) throws WebException {
		String query = "UPDATE users Set isBanned=0 where email=? and position_id<3;";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

	}
	/**
	 * Adds a book in database
	 * @param book
	 * @throws WebException
	 */
	public void addBook(Book book) throws WebException {
		String query = "INSERT INTO `books` (`name`, `visitors`, `authors_id`, `book_number`) VALUES (?, ?, ?, ?);";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, book.getName());
			stmt.setInt(2, book.getVisits());
			stmt.setInt(3, book.getAuthor().getId());
			stmt.setInt(4, book.getBookNumber());

			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

	}
	/**
	 * Checks if book exists in database
	 * @param book
	 * @return true if book exist, false if not
	 * @throws WebException
	 */
	public boolean checkBook(Book book) throws WebException {
		String query = "SELECT EXISTS(SELECT id FROM books WHERE name = ? and authors_id = ?)";
		Connection con = getConnection();
		boolean res = false;
		ResultSet rs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, book.getName());
			stmt.setInt(2, book.getAuthor().getId());
			rs = stmt.executeQuery();
			while (rs.next()) {
				res = rs.getInt(1) == 1;
				break;
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return res;
	}
	/**
	 * Searches a book by id
	 * @param id
	 * @return Book
	 * @throws WebException
	 */
	public Book findBookById(int id) throws WebException {
		String query = "SELECT\r\n" + "	books.id as book_id, books.name as book_name, book_number as book_number,\r\n"
				+ "	authors.id as author_id, authors.name as author_name, authors.surname as author_surname\r\n"
				+ "FROM books \r\n" + "INNER JOIN authors ON authors.id=books.authors_id \r\n" + "where books.id = ?;";
		Book book = null;
		ResultSet rs = null;
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			while (rs.next()) {
				book = Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(4), rs.getString(5), rs.getString(6)), rs.getInt(3));
				break;
			}

			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
			close(rs);
		}
		return book;

	}
	/**
	 * Edits selected book
	 * @param bookChanged
	 * @return	true
	 * @throws WebException
	 */
	public boolean editBook(Book bookChanged) throws WebException {

		String query = "UPDATE `mydb`.`books` SET `name` = ?, `authors_id` = ?, `book_number` = ? WHERE (`id` = ?);";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, bookChanged.getName());
			stmt.setInt(2, bookChanged.getAuthor().getId());
			stmt.setInt(3, bookChanged.getBookNumber());
			stmt.setInt(4, bookChanged.getId());
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

		return true;

	}
	/**
	 * Adds order into database
	 * @param book
	 * @param userId
	 * @return true
	 * @throws WebException
	 */
	public boolean addTicket(Book book, int userId) throws WebException {
		String query = "INSERT INTO `ticket` (`user_id`, `book_id`, `status`, `date`) VALUES (?, ?, 'NEW', ?);";
		Connection con = getConnection();

		try (PreparedStatement stmt = con.prepareStatement(query);) {

			LocalDateTime time = LocalDateTime.now().plusMonths(1);
			Timestamp timestamp = Timestamp.valueOf(time);
			stmt.setInt(1, userId);
			stmt.setInt(2, book.getId());
			stmt.setTimestamp(3, timestamp);
			stmt.execute();
			con.commit();

		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return true;
	}
	/**
	 * Updates user position to 2(librarian pos) if current position is not 3(admin position)
	 * @param email
	 * @throws WebException
	 */
	public void createLibrarian(String email) throws WebException {
		String query = "UPDATE users Set position_id=2 where email=? and position_id!=3;";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

	}
	/**
	 * Updates user position to 1(user pos) if current position is 2(librarian position)
	 * @param email
	 * @throws WebException
	 */
	public void deleteLibrarian(String email) throws WebException {
		String query = "UPDATE users Set position_id=1 where email=? and position_id=2;";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, email);
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

	}
	/**
	 * Searches for orders from selected user
	 * @param userId
	 * @return List of orders
	 * @throws WebException
	 */
	public List<Subscription> findUserSubs(int userId) throws WebException {
		List<Subscription> subs = new ArrayList<>();
		String query = "SELECT *\r\n" + "  FROM ticket\r\n" + "LEFT OUTER JOIN users\r\n"
				+ "  ON ticket.user_id = users.id\r\n" + "LEFT OUTER JOIN books\r\n"
				+ "  ON ticket.book_id = books.id\r\n" + "LEFT OUTER JOIN authors\r\n"
				+ "	ON books.authors_id = authors.id\r\n" + "    where user_id = ? order by ticket.status";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, userId);
			try (ResultSet rs = stmt.executeQuery()) {

				while (rs.next()) {
					String ban;
					if (rs.getInt(13) == 0) {
						ban = "No";
					} else {
						ban = "Yes";
					}
					User user = User.createUser(rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10),
							rs.getInt(12), ban);
					Book book = Book.createBook(rs.getInt(14), rs.getString(15),
							Author.createAuthor(rs.getInt(19), rs.getString(20), rs.getString(21)), rs.getInt(18));
					if (rs.getString(4).equals("APROVED")) {
						LocalDateTime current = LocalDateTime.now();
						LocalDateTime time = LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime());
						if (time.isBefore(current) && rs.getInt(6) == 0) {
							addFee(rs.getInt(1), con);
						}
					}
					subs.add(Subscription.createSubscription(rs.getInt(1), user, book, rs.getString(4),
							LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime()), rs.getInt(6)));

				}
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return subs;
	}
	/**
	 * Adds fee to order
	 * @param id
	 * @param con
	 * @throws SQLException
	 */
	private void addFee(int id, Connection con) throws SQLException {
		String query = "UPDATE `ticket` SET `fee` = '100' WHERE (`id` = ?);";
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			stmt.execute();
		}
	}
	/**
	 * Removes fee from order
	 * @param id
	 * @param con
	 * @throws SQLException
	 */
	private void removeFee(int id, Connection con) throws SQLException {
		String query = "UPDATE `ticket` SET `fee` = 0 WHERE (`id` = ?);";
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			stmt.execute();
		}
	}
	/**
	 * Check if you can modify this order
	 * @param id
	 * @param status
	 * @return true if you can modify order, flase in opposite
	 * @throws WebException
	 */
	public boolean checkSubStatus(int id, String status) throws WebException {
		String query = "SELECT * FROM mydb.ticket where id = ?;";
		Connection con = getConnection();
		boolean res = false;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, id);
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					String originStatus = rs.getString(4);
					if (originStatus.equals("NEW")) {
						res = true;
					}
					if (originStatus.equals("APROVED") && !status.equals("DECLINED") && !status.equals("NEW") && !status.equals("APROVED")) {
						res = true;
					}
					break;
				}
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			e.printStackTrace();

		} finally {
			close(con);
		}
		return res;

	}
	/**
	 * Edits order status
	 * @param id
	 * @param status
	 * @param bookId
	 * @throws WebException
	 */
	public void editSubStatus(int id, String status, int bookId) throws WebException {
		String query = "UPDATE `ticket` SET `status` =? WHERE (`id` = ?)";
		Connection con = getConnection();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setString(1, status);
			stmt.setInt(2, id);
			if (status.equals("APROVED")) {
				if(checkBookNumber(bookId, con)) {
					reduceBookNumber(bookId, con);
					
				}
				else {
					throw new WebException("You cant aprrove this order due to lack of book");
				}
			} else {
				if (status.equals("ENDED")) {
					removeFee(id, con);
					returnBookNumber(bookId, con);
				}
				if (status.equals("DECLINED")) {
					removeFee(id, con);
				}
			}
			stmt.execute();
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operationssss. Your request will rollback");

		} finally {
			close(con);
		}

	}
	/**
	 * Adds 1 to book number of selected book
	 * @param bookId
	 * @param con
	 * @throws WebException
	 * @throws SQLException
	 */
	private void returnBookNumber(int bookId, Connection con) throws WebException, SQLException {
		String query = "UPDATE `books` SET `book_number` = book_number+1 WHERE (`id` = ?);";
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, bookId);
			stmt.execute();
		}
	}
	/**
	 * Checks is there a selected books with book number bigger than 0
	 * @param bookId
	 * @param con
	 * @return
	 * @throws WebException
	 * @throws SQLException
	 */
	private boolean checkBookNumber(int bookId, Connection con) throws WebException, SQLException {
		String query = "SELECT * FROM mydb.books where id = ? and book_number > 0;";
		boolean res = false;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, bookId);
			try (ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					res = true;
					break;
				}
			}
			
		}
		return res;
	}
	/**
	 * Reduces a book number of selected book
	 * @param bookId
	 * @param con
	 * @throws WebException
	 * @throws SQLException
	 */
	private void reduceBookNumber(int bookId, Connection con) throws WebException, SQLException {
		String query = "UPDATE `books` SET `book_number` = book_number-1, `visitors` = visitors+1  WHERE (`id` = ?) and `book_number` > 0;";
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, bookId);
			stmt.execute();
		}
	}
	/**
	 * Searches for Subscriptions with selected id
	 * @param ticketId
	 * @return Subscription
	 * @throws WebException
	 */
	public Subscription getTicketById(int ticketId) throws WebException {
		String query = "SELECT *\r\n" + "  FROM ticket\r\n" + "LEFT OUTER JOIN users\r\n"
				+ "  ON ticket.user_id = users.id\r\n" + "LEFT OUTER JOIN books\r\n"
				+ "  ON ticket.book_id = books.id\r\n" + "LEFT OUTER JOIN authors\r\n"
				+ "	ON books.authors_id = authors.id\r\n" + "    where id = ?";
		Connection con = getConnection();
		Subscription subs = null;
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			stmt.setInt(1, ticketId);
			try (ResultSet rs = stmt.executeQuery()) {

				while (rs.next()) {
					String ban;
					if (rs.getInt(13) == 0) {
						ban = "No";
					} else {
						ban = "Yes";
					}
					User user = User.createUser(rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10),
							rs.getInt(12), ban);
					Book book = Book.createBook(rs.getInt(14), rs.getString(15),
							Author.createAuthor(rs.getInt(19), rs.getString(20), rs.getString(21)), rs.getInt(18));
					if (rs.getString(4).equals("APROVED")) {
						LocalDateTime current = LocalDateTime.now();
						LocalDateTime time = LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime());
						if (time.isBefore(current) && rs.getInt(6) == 0) {
							addFee(rs.getInt(1), con);
						}
					}
					subs = (Subscription.createSubscription(rs.getInt(1), user, book, rs.getString(4),
							LocalDateTime.of(rs.getDate(5).toLocalDate(), rs.getTime(5).toLocalTime()), rs.getInt(6)));
					break;

				}
			}
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}
		return subs;
	}
	public List<Book> findBooksWithAuthors() throws WebException {
		String query = "SELECT\r\n" + "	books.id as book_id, books.name as book_name, book_number as book_number,\r\n"
				+ "	authors.id as author_id, authors.name as author_name, authors.surname as author_surname\r\n"
				+ "FROM books\r\n" + "INNER JOIN authors ON authors.id=books.authors_id order by authors_id";
		Connection con = getConnection();
		List<Book> books = new ArrayList<>();
		try (PreparedStatement stmt = con.prepareStatement(query);) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				books.add(Book.createBook(rs.getInt(1), rs.getString(2),
						Author.createAuthor(rs.getInt(4), rs.getString(5), rs.getString(6)), rs.getInt(3)));
			}
		} catch (SQLException e) {
			rollback(con);
			throw new WebException("Error with operation. Your request will rollback");
		} finally {
			close(con);
		}

		return books;
	}

	/**
	 * closes ResultSet
	 * 
	 * @param rs ResultSet
	 */

	private void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				LOG.error("Cannot close resultset", e);
			}
		}
	}

	/**
	 * closes Connection
	 * 
	 * @param con Connection
	 */

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				LOG.error("Cannot close connection", e);
			}
		}
	}

	/**
	 * rollback, if transaction was failed
	 * 
	 * @param con Connection
	 */

	private void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				LOG.error("Cannot rollback transaction", e);
			}
		}
	}

}
