package exception;
/**
 * WebException
 * @author Nikita Trufanov
 *
 */
public class WebException extends Exception {
	
	private static final long serialVersionUID = 8288779062647218916L;

	public WebException() {
		super();
	}

	public WebException(String message, Throwable cause) {
		super(message, cause);
	}

	public WebException(String message) {
		super(message);
	}

}
