package listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

/**
 * Application Lifecycle Listener implementation class AppSessionListener
 *
 */
@WebListener
public class AppSessionListener implements HttpSessionListener {
	private static final Logger LOG = Logger.getLogger(AppSessionListener.class);
    /**
     * Default constructor. 
     */
    public AppSessionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent se)  { 
    	LOG.debug("Session Created:: ID="+se.getSession().getId());
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se)  { 
    	LOG.debug("Session Destroyed:: ID="+se.getSession().getId());
    }
	
}
