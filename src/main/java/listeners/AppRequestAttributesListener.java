package listeners;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

/**
 * Application Lifecycle Listener implementation class AppRequestAttributesListener
 *
 */
@WebListener
public class AppRequestAttributesListener implements ServletRequestAttributeListener {
	private static final Logger LOG = Logger.getLogger(AppRequestAttributesListener.class);
    /**
     * Default constructor. 
     */
    public AppRequestAttributesListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletRequestAttributeListener#attributeRemoved(ServletRequestAttributeEvent)
     */
    public void attributeRemoved(ServletRequestAttributeEvent srae)  { 
    	LOG.debug("ServletContext attribute removed::{"+srae.getName()+","+srae.getValue()+"}");
    }

	/**
     * @see ServletRequestAttributeListener#attributeAdded(ServletRequestAttributeEvent)
     */
    public void attributeAdded(ServletRequestAttributeEvent srae)  { 
    	LOG.debug("ServletContext attribute added::{"+srae.getName()+","+srae.getValue()+"}");
    }

	/**
     * @see ServletRequestAttributeListener#attributeReplaced(ServletRequestAttributeEvent)
     */
    public void attributeReplaced(ServletRequestAttributeEvent srae)  { 
    	LOG.debug("ServletContext attribute replaced::{"+srae.getName()+","+srae.getValue()+"}");
    }
	
}
