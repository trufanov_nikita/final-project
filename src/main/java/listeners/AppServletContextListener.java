package listeners;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 * Application Lifecycle Listener implementation class List
 *
 */
@WebListener
public class AppServletContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public AppServletContextListener() {
        // TODO Auto-generated constructor stub
    }

    private static final Logger LOG = Logger.getLogger(AppServletContextListener.class);

	public void contextDestroyed(ServletContextEvent event) {
		
		LOG.debug("Servlet context destruction starts");
		LOG.debug("Servlet context destruction finished");
	}

	public void contextInitialized(ServletContextEvent event) {
		
		LOG.debug("Servlet context initialization starts");

		ServletContext servletContext = event.getServletContext();
		initLog4J(servletContext);
		
		LOG.debug("Servlet context initialization finished");
	}

	/**
	 * Initializes log4j framework.
	 * 
	 * @param servletContext
	 */
	private void initLog4J(ServletContext servletContext) {
		LOG.debug("Log4J initialization started");
		try {
			PropertyConfigurator.configure(
				servletContext.getRealPath("WEB-INF/log4j.properties"));
			LOG.debug("Log4j has been initialized");
		} catch (Exception ex) {
			LOG.error("Cannot configure Log4j", ex);
		}		
		LOG.debug("Log4J initialization finished");
	}
	
}
