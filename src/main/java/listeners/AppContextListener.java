package listeners;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import controller.ControllerAdmin;

/**
 * Application Lifecycle Listener implementation class AppContextListener
 *
 */
@WebListener
public class AppContextListener implements ServletContextAttributeListener {
	private static final Logger LOG = Logger.getLogger(ControllerAdmin.class);
	/**
     * @see ServletContextAttributeListener#attributeAdded(ServletContextAttributeEvent)
     */
    public void attributeAdded(ServletContextAttributeEvent scae)  { 
    	
    	LOG.debug("ServletContext attribute added::{"+scae.getName()+","+scae.getValue()+"}");
    }

	/**
     * @see ServletContextAttributeListener#attributeRemoved(ServletContextAttributeEvent)
     */
    public void attributeRemoved(ServletContextAttributeEvent scae)  { 
    	LOG.debug("ServletContext attribute removed::{"+scae.getName()+","+scae.getValue()+"}");
    	
    }

	/**
     * @see ServletContextAttributeListener#attributeReplaced(ServletContextAttributeEvent)
     */
    public void attributeReplaced(ServletContextAttributeEvent scae)  { 
    	LOG.debug("ServletContext attribute replaced::{"+scae.getName()+","+scae.getValue()+"}");
    }

	
}
