package listeners;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

/**
 * Application Lifecycle Listener implementation class AppRequestListener
 *
 */
@WebListener
public class AppRequestListener implements ServletRequestListener {
	private static final Logger LOG = Logger.getLogger(AppRequestListener.class);
    /**
     * Default constructor. 
     */
    public AppRequestListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletRequestListener#requestDestroyed(ServletRequestEvent)
     */
    public void requestDestroyed(ServletRequestEvent sre)  { 
    	ServletRequest servletRequest = sre.getServletRequest();
    	LOG.debug("ServletRequest destroyed. Remote IP="+servletRequest.getRemoteAddr());
    }

	/**
     * @see ServletRequestListener#requestInitialized(ServletRequestEvent)
     */
    public void requestInitialized(ServletRequestEvent sre)  { 
    	ServletRequest servletRequest = sre.getServletRequest();
    	LOG.debug("ServletRequest initialized. Remote IP="+servletRequest.getRemoteAddr());
    }
	
}
