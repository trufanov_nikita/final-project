package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Servlet Filter implementation class Filter1
 */
@WebFilter()
public class FilterEncoding extends HttpFilter implements Filter {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2939511767741801010L;
	private final static String ENCODING_UTF_8 = "UTF-8";
	private static final Logger LOG = Logger.getLogger(FilterEncoding.class);
    /**
     * @see HttpFilter#HttpFilter()
     */
    public FilterEncoding() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		LOG.debug("Filter destroyed");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		LOG.debug("Filter starts");
		HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        LOG.trace("Request uri " + request.getRequestURI());
        HttpSession session = request.getSession();
        if(session.getAttribute("lang") == null) {
        	session.setAttribute("lang", "ua");
        }
        request.setCharacterEncoding(ENCODING_UTF_8);
        response.setCharacterEncoding(ENCODING_UTF_8);
        LOG.trace("Request encoding = null, set encoding " + ENCODING_UTF_8);
		// pass the request along the filter chain
        LOG.debug("Filter ends");
		chain.doFilter(request, response);
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.debug("Filter initialization starts");
		LOG.debug("Filter initialization finished");
	}

}
