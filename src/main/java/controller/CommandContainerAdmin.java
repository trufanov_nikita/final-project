package controller;

import java.util.Map;
import java.util.TreeMap;

import command.AddBook;
import command.AddLibrarian;
import command.AdminInterface;
import command.Ban;
import command.ClientOrder;
import command.Command;
import command.EditBook;
import command.EditChosenBook;
import command.EditSubscription;
import command.FindAuthors;
import command.FindBooks;
import command.GetBooks;
import command.GetUsers;
import command.Login;
import command.MostPopular;
import command.RedirectToInterface;
import command.Register;
import command.RemoveLibrarian;
import command.Search;
import command.UnBan;
import command.UserSubscriptions;
/**
 * 
 * @author Nikita Trufanov
 * Represents a container with available command of admin
 */
public class CommandContainerAdmin {
	private CommandContainerAdmin() {
		
	}
	private static Map<String, Command> commands = new TreeMap<String, Command>();

	static {
		commands.put("addBook", new AddBook());
		commands.put("mostPopular", new MostPopular());
		commands.put("addLibrarian", new AddLibrarian());
		commands.put("adminInterface", new AdminInterface());
		commands.put("ban", new Ban());
		commands.put("clientOrder", new ClientOrder());
		commands.put("editBook", new EditBook());
		commands.put("editChosenBook", new EditChosenBook());
		commands.put("editSubscription", new EditSubscription());
		commands.put("authors", new FindAuthors());
		commands.put("books", new FindBooks());
		commands.put("login", new Login());
		commands.put("redirectToInterface", new RedirectToInterface());
		commands.put("register", new Register());
		commands.put("removeLibrarian", new RemoveLibrarian());
		commands.put("search", new Search());
		commands.put("unBan", new UnBan());
		commands.put("userSubscriptions", new UserSubscriptions());
		commands.put("findBooksLimit", new GetBooks());
		commands.put("findUsersLimit", new GetUsers());
		
	}
	/**
	 * 
	 * @param command String representation of needed command
	 * @return requested command if existed
	 */
	public static Command get(String command) {
		return commands.get(command);
	}

}
