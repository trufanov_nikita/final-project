package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import command.Command;
import exception.WebException;

/**
 * 
 * @author Nikita Trufanov
 * Represents a Servlet witch takes a command from request and executes it
 *
 */
@WebServlet("/ControllerAdmin")
public class ControllerAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final org.apache.log4j.Logger LOG = Logger.getLogger(ControllerAdmin.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerAdmin() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	LOG.trace("DoGet");
		process(request, response);
		
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOG.trace("DoPost");
		process(request, response);
		

	}

	/**
	 * method gets command from command container
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String forward = PathContainer.ERROR_PAGE;
		String commandName = request.getParameter("command");
		HttpSession session = request.getSession();
		
		if(session.getAttribute("position") == null || !session.getAttribute("position").equals(3)) {
			request.getRequestDispatcher(PathContainer.DEFAULT).forward(request,
					response);
			request.getSession().invalidate();
			return;
		}
		
		Command command = CommandContainerAdmin.get(commandName);
		LOG.debug("command: " + forward);
		try {
			forward=command.execute(request, response);
		} catch (WebException e) {
			if (e.getCause() != null) {
				request.setAttribute("errorMessage", e.getMessage() + ": "
						+ e.getCause().getMessage());
			} else {
				request.setAttribute("errorMessage", e.getMessage());
			}
		}

		request.getRequestDispatcher(forward).forward(request, response);
	}

}
