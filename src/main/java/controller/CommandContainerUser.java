package controller;

import java.util.Map;
import java.util.TreeMap;

import command.ClientOrder;
import command.Command;

import command.FindAuthors;
import command.FindBooks;
import command.GetCatalog;
import command.Login;
import command.MostPopular;
import command.RedirectToInterface;
import command.Register;
import command.Search;
import command.UserInterface;
import command.UserSubscriptions;
/**
 * 
 * @author Nikita Trufanov
 * Represents a container with available commands for user
 */
public class CommandContainerUser {
	private CommandContainerUser() {
		
	}
	private static Map<String, Command> commands = new TreeMap<String, Command>();

	static {
		commands.put("mostPopular", new MostPopular());
		commands.put("clientOrder", new ClientOrder());
		commands.put("authors", new FindAuthors());
		commands.put("books", new FindBooks());
		commands.put("login", new Login());
		commands.put("redirectToInterface", new RedirectToInterface());
		commands.put("register", new Register());
		commands.put("search", new Search());
		commands.put("userInterface", new UserInterface());
		commands.put("userSubscriptions", new UserSubscriptions());
		commands.put("getCatalog", new GetCatalog());
	}
	/**
	 * 
	 * @param command String representation of needed command
	 * @return requested command if existed
	 */
	public static Command get(String command) {
		return commands.get(command);
	}

}
