package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import command.Command;
import exception.WebException;

/**
 * 
 * @author Nikita Trufanov
 * Represents a Servlet witch takes a command from request and executes it
 *
 */
@WebServlet("/ControllerGuest")
public class ControllerGuest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final org.apache.log4j.Logger LOG = Logger.getLogger(ControllerGuest.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerGuest() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	LOG.trace("DoGet");
		process(request, response);
		
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOG.trace("DoPost");
		process(request, response);
		

	}

	/**
	 * method gets command from command container
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String forward = PathContainer.ERROR_PAGE;
		String commandName = request.getParameter("command");
		Command command = CommandContainerUser.get(commandName);
		HttpSession session = request.getSession();

		try {
			forward=command.execute(request, response);
			LOG.debug("command: " + forward);
		} catch (WebException e) {
			if (e.getCause() != null) {
				request.setAttribute("errorMessage", e.getMessage() + ": "
						+ e.getCause().getMessage());
			} else {
				request.setAttribute("errorMessage", e.getMessage());
			}
		}

		request.getRequestDispatcher(forward).forward(request, response);
	}

}
