package controller;


/**
 * Represents a container that contains paths for pages
 * @author Nikita Trudanov
 *
 */
public class PathContainer {

	public static final String DEFAULT = "index.jsp";
	public static final String SUCCESS = "user_pages/Success.jsp";
	public static final String ADMIN = "user_pages/Admin.jsp";
	public static final String CATALOG = "user_pages/CatalogLimit.jsp";
	public static final String EDIT = "user_pages/Edit.jsp";
	public static final String LIBRARIAN = "user_pages/librarian.jsp";
	public static final String LIBRARIAN_SUBSCRIPTION = "user_pages/LibrarianSubscription.jsp";
	public static final String FIND_USER_SUBS = "user_pages/FindUserSubs.jsp";
	public static final String SUBSCRIPTIONS = "user_pages/Subscriptions.jsp";
	public static final String USER = "user_pages/User.jsp";
	public static final String ERROR_PAGE = "error.jsp";
	public static final String PROFILE = "profile/Admin.jsp";
	public static final String AUTHORS = "main_page/authors.jsp";
	public static final String BOOKS = "main_page/books.jsp";
	public static final String SEARCH = "main_page/search.jsp";
	public static final String ADMIN_BOOKS = "user_pages/BooksLimit.jsp";
	public static final String ADMIN_USERS = "user_pages/UserLimit.jsp";
	public static final String LIBRARIAN_TICKETS = "user_pages/TicketLimit.jsp";
}
