package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.User;
import controller.PathContainer;
import classes.Subscription;
import database.DBManager;
import exception.WebException;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Redirect")
public class Redirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Redirect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page;
		DBManager db = DBManager.createDb();
		HttpSession session = request.getSession();
		Object positionTemp = session.getAttribute("position");
		if(positionTemp == null || !positionTemp.toString().matches("[-+]?\\d+")){
			request.getRequestDispatcher(PathContainer.DEFAULT).forward(request, response);
		}
		int position = (int)positionTemp;
		String email = (String) session.getAttribute("email");
		if (email == null || email.isBlank()) {
			request.getRequestDispatcher(PathContainer.DEFAULT).forward(request, response);
		}
		try {
			if (db.userBanned(email) == 0) {

				switch (position) {
				case 1:
					session.setAttribute("position_name", "User");
					request.getRequestDispatcher(PathContainer.USER).forward(request, response);
					break;
				case 2:
					session.setAttribute("position_name", "Librarian");
					request.getRequestDispatcher(PathContainer.LIBRARIAN).forward(request, response);
					break;
				case 3:
					session.setAttribute("position_name", "Admin");
					request.getRequestDispatcher(PathContainer.ADMIN).forward(request, response);
					break;
				default:
					request.getRequestDispatcher(PathContainer.DEFAULT).forward(request, response);
				}

			} else {

				request.getRequestDispatcher(PathContainer.DEFAULT).forward(request, response);
			}
		} catch (WebException e ) {
			// TODO Auto-generated catch block
			request.getRequestDispatcher(PathContainer.DEFAULT).forward(request, response);
		}

	}
				

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
