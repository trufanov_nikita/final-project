package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.User;
import classes.Author;
import classes.Book;
import classes.Subscription;
import database.DBManager;
import exception.WebException;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Profile() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd;
		DBManager db = DBManager.createDb();

		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");
		if (email != null) {
			User user;
			try {
				user = db.findUser(email);
				request.setAttribute("user", user);
				int pos = user.getPosition();
				String position;
				switch (pos) {
				case 1:
					position = "User";
					break;
				case 2:
					position = "Librarian";
					break;
				case 3:
					position = "Admin";
					break;
				default:
					position = "User";
					break;
				}
				List<Subscription> subscriptions = db.findUserSubs(user.getId());
				List<Author> authors = new ArrayList<>();
				subscriptions.stream().forEach(x -> authors.add(x.getBook().getAuthor()));
				Comparator<Subscription> bySubscription = (first, second) -> Integer.compare(
						Collections.frequency(subscriptions, first), Collections.frequency(subscriptions, second));
				Comparator<Author> byAuthor = (first, second) -> Integer.compare(Collections.frequency(authors, first),
						Collections.frequency(authors, second));
				Subscription sub = subscriptions.stream().max(bySubscription).orElse(null);
				Author author = authors.stream().max(byAuthor).orElse(null);
				long fee = subscriptions.stream().collect(Collectors.summarizingInt(Subscription::getFee)).getSum();
				long count = subscriptions.stream().count();
				long countOnHands = subscriptions.stream().filter(x -> x.getStatus().equals("APROVED")).count();
				request.setAttribute("position", position);
				request.setAttribute("fee", fee);
				request.setAttribute("count", count);
				request.setAttribute("countOnHands", countOnHands);
				request.setAttribute("sub", sub);
				request.setAttribute("author", author);
			} catch (WebException e) {
				if (e.getCause() != null) {
					request.setAttribute("errorMessage", e.getMessage() + ": " + e.getCause().getMessage());
				} else {
					request.setAttribute("errorMessage", e.getMessage());
				}
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
		request.getRequestDispatcher("profile/profile.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
