<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<head>
<meta charset="UTF-8">
<title>My test</title>
<c:if test="condition"></c:if>
<link rel="stylesheet" href="index.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap"
	rel="stylesheet">
<link href="http://fonts.cdnfonts.com/css/roboto" rel="stylesheet">
<script src="./script.js" defer></script>
</head>

<body class="body">

	<header class="header">
		<div class="menu">

			<div class="options" style="padding-left: 17vh;">
				<c:if test="${sessionScope.lang eq 'en'}">

					<form action="ChangeLanguage" method="post">
						<button>UA</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_en.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>


				</c:if>
				<c:if test="${sessionScope.lang eq 'ua'}">
					<form action="ChangeLanguage" method="post">
						<button>EN</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_ua.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>

				</c:if>


			</div>

			<div class="library">
				<span class="library_text"><a href="index.jsp"><fmt:message
							key="label.homepage" /></a></span> <img src="./assets/library_image.png"
					alt="library_image" class="library_image">
			</div>
			<div class="registration">
				<%
				String username = (String) request.getSession().getAttribute("name");
				%>
				<button class="registration_button" id="registration_id">
					<span class="registrarion_text"> <%
 if (username == null) {
 %> <fmt:message key="label.register" /> <%
 } else {
 %> <fmt:message key="label.create_new" /> <%
 }
 %>
					</span>

				</button>
				<span class="slash">/</span>
				<button class="registration_button" id="login_id">
					<span class="registrarion_text" id="sign_in"> <%
 username = (String) request.getSession().getAttribute("name");
 %> <%
 if (username == null) {
 %><fmt:message key="label.login" /> <%
 } else {
 %><fmt:message key="label.login" /> <%
 }
 %></span>
				</button>





				<div class="account">
					<a href="Profile" class="account_button">
						<div class="image">
							<img src="./assets/profile_image.png" alt="profile_logo"
								class="profile_image" width="30" height="30">
						</div>
					</a>
				</div>
			</div>

		</div>
	</header>
	<main class="main">
		<div class="main_wrapper">
			<div class="forms" id="rigistration_form_id">
				<div class="rigistration_form">
					<form action="ControllerGuest" class="login" method="get">
						<input type="hidden" name="command" value="register">
						<div class="inputs">
							<label for="name" class="label_input"><fmt:message
									key="label.name" /></label> <input class="form-input" type="text"
								id="name" name="name" placeholder="Your Name" required>
						</div>
						<div class="inputs">
							<label for="surname" class="label_input"><fmt:message
									key="label.surname" /></label> <input class="form-input" type="text"
								id="surname" name="surname" placeholder="Your Surname" required>
						</div>
						<div class="inputs">
							<label for="email" class="label_input"><fmt:message
									key="label.email" /></label> <input class="form-input" type="text"
								id="email" name="email" placeholder="Your Email" required>
						</div>
						<div class="inputs">
							<label for="password" class="label_input"><fmt:message
									key="label.password" /></label> <input class="form-input"
								type="password" id="password" name="password"
								placeholder="Create Password" required>
							<div class="submit_registrarion">
								<button class="submit_button">
									<span class="submit_button_text"><fmt:message
											key="label.submit" /></span>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="forms_login">
				<div class="login_form" id="forms_login_id">
					<form action="ControllerGuest" class="login" method="post">
						<input type="hidden" name="command" value="login">
						<div class="inputs">
							<label for="email" class="label_input"><fmt:message
									key="label.email" /></label> <input class="form-input" type="text"
								id="email" name="email" placeholder="Your Email" required>
						</div>
						<div class="inputs">
							<label for="password" class="label_input"><fmt:message
									key="label.password" /></label> <input class="form-input"
								type="password" id="password" name="password"
								placeholder="Your Password" required>
						</div>
						<div class="inputs">
							<div class="submit_registrarion">
								<button class="submit_button">
									<span class="submit_button_text"><fmt:message
											key="label.submit_login" /></span>
								</button>
							</div>
						</div>

					</form>
				</div>
				<div class="search_wrapper_main" id="search_id">
					<div class="searchbar_wrapper">
						<form action="ControllerGuest" class="search" method="post">
							<input type="hidden" name="command" value="search">
							<div class="submit_search_button">
								<button type="submit" class="submit">
									<div class="submit_wrapper">
										<img src="assets/submit_button.png" alt="submit"
											class="submit_image">
									</div>
								</button>
								<input type="text" class="search_input" id="searchid"
									name="search_input"
									placeholder="<fmt:message
									key="label.search_under" />">
								<div class="radio_input">
									<div class="radio_input_inner">
										<input type="radio" id="contactChoice1" name="radio_input"
											value="book" checked> <label for="contactChoice1">Book</label>
									</div>
									<div class="radio_input_inner">
										<input type="radio" id="contactChoice2" name="radio_input"
											value="author"> <label for="contactChoice2">Author</label>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="under_search">
						<div class="under_search_bar">

							<form action="ControllerGuest" method="post">
								<input type="hidden" name="command" value="authors">
								<button>
									<span class="authors_search"><fmt:message
											key="label.authors" /></span>
								</button>
							</form>
							<form action="ControllerGuest" method="post">
								<input type="hidden" name="command" value="books">
								<button>
									<span class="authors_search"><fmt:message
											key="label.books" /></span>
								</button>
							</form>
							<form action="ControllerGuest" method="post">
								<input type="hidden" name="command" value="mostPopular">
								<button>
									<span class="authors_search"><fmt:message
											key="label.popular" /></span>
								</button>
							</form>
						</div>
						<div class="credentials">
							<div class="credential">

								<div class="credential_text">
									<span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span>
								</div>
							</div>
							<div class="credential">

								<div class="credential_text">
									<span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span>
								</div>
							</div>
							<div class="credential">

								<div class="credential_text">
									<span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>

									</span> <span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span>
								</div>
							</div>
							<div class="credential">

								<div class="credential_text">
									<span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span> <span class="credential_text_inner"> <a href=""
										class="credential_link" id="credential_id">Some text</a>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</body>