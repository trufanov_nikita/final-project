<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./index.css">
<title>Librarian</title>
</head>
<body class="body">
	<header class="header">
		<div class="menu">

			<div class="options">
				<c:if test="${sessionScope.lang eq 'en'}">

					<form action="ChangeLanguage" method="post">
						<button>UA</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_en.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>


				</c:if>
				<c:if test="${sessionScope.lang eq 'ua'}">
					<form action="ChangeLanguage" method="post">
						<button>EN</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_ua.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>

				</c:if>


			</div>

			<div class="library">
				<span class="library_text"><a href="index.jsp"><fmt:message
							key="label.homepage" /></a></span> <img src="./assets/library_image.png"
					alt="library_image" class="library_image">
			</div>



			<div class="account">
				<a href="Profile" class="account_button"> <img
					src="./assets/profile_image.png" alt="profile_logo"
					class="profile_image" width="30" height="30">
				</a>
			</div>
		</div>
	</header>
	<main class="main">
		<div class="main_wrapper">
			<form action="ControllerLibrarian" class="search" method="post">
				<input type="hidden" name="command" value="subsLimit"> <input
					type="hidden" name="page" value="1">
				<div class="submit_search_button">
					<button type="submit" class="submit">
						<div class="submit_wrapper">
							<fmt:message key="librarian.subs" />
						</div>
					</button>
				</div>
			</form>
			<div class="search_wrapper_main" id="search_id">
				<div class="searchbar_wrapper">
					<form action="ControllerLibrarian" class="search" method="post">
						<input type="hidden" name="command" value="searchUserSubscriptions">
						<div class="submit_search_button">
							<button type="submit" class="submit">
								<div class="submit_wrapper">
									<img src="assets/submit_button.png" alt="submit"
										class="submit_image">
								</div>
							</button>
							<input type="text" class="search_input" id="searchid"
								name="search_input"
								placeholder="<fmt:message
						key="label.search_under" />">
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>
</body>
</html>