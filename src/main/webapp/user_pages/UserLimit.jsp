<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<html>

<head>
<link rel="stylesheet" href="user_css/table.css">
<title>Books</title>
</head>
<body>
	<form action="ControllerAdmin" class="unban" method="post"
		style="display: flex; justify-content: center;">
		<input type="hidden" name="command" value="adminInterface">
		<button>AdminInterface</button>
	</form>
	<span><fmt:message key="current_page" /> ${currentPage}</span>
	<table id="1" class="table_sort">
		<thead>
			<tr>
				<!--When a header is clicked, run the sortTable function, with a parameter, 0 for sorting by names, 1 for sorting by country:-->
				<th><fmt:message key="table.id" /></th>
				<th><fmt:message key="table.user.name" /></th>
				<th><fmt:message key="table.user.surname" /></th>
				<th><fmt:message key="table.user.email" /></th>
				<th><fmt:message key="table.user.pos" /></th>
				<th><fmt:message key="table.user.ban" /></th>
				<th>Ban</th>
				<th>Unban</th>
				<th>Add Librarian</th>
				<th>Delete Librarian</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list_users}" var="list">
				<tr>
					<td>${list.id}</td>
					<td>${list.name}</td>
					<td>${list.surname}</td>
					<td>${list.email}</td>
					<td><c:choose>
							<c:when test="${list.position == 1}">
								<fmt:message key="position.user" />
							</c:when>
							<c:when test="${list.position == 2}">
								<fmt:message key="position.librarian" />
							</c:when>
							<c:when test="${list.position == 3}">
								<fmt:message key="position.admin" />
							</c:when>
							<c:otherwise>undefined</c:otherwise>
						</c:choose></td>
					<td>${list.ban}</td>
					<td>
						<form action="ControllerAdmin" class="ban" method="post">
							<input type="hidden" name="command" value="ban">
							<div class="ban_button_wrapper">
								<button type="submit" class="ban_i">
									<i class="i_text">BAN</i>
								</button>
								<input type="hidden" class="ban_input" id="banid"
									name="ban_input" value="${list.email}">
							</div>
						</form>
					</td>
					<td><form action="ControllerAdmin" class="unban" method="post">
							<input type="hidden" name="command" value="unBan">
							<div class="unban_search_button">
								<button type="submit" class="unban_i">
									<i class="i_text">Unban</i>
								</button>
								<input type="hidden" class="unban_input" id="unbanid"
									name="unban_input" value="${list.email}">
							</div>
						</form></td>
					<td>
						<form action="ControllerAdmin" class="add_librarian" method="post">
							<input type="hidden" name="command" value="addLibrarian">
							<div class="librarian_add_button">
								<button type="submit" class="librarian_add_i">
									<i class="i_text">Add</i>
								</button>
								<input type="hidden" class="librarian_input" id="librarianId"
									name="librarian_input" value="${list.email}">
							</div>
						</form>
					</td>
					<td>
						<form action="ControllerAdmin" class="remove_librarian"
							method="post">
							<input type="hidden" name="command" value="removeLibrarian">
							<div class="librarian_remove_button">
								<button type="submit" class="librarian_remove_i">
									<i class="i_text">Delete</i>
								</button>
								<input type="hidden" class="librarian_input" id="librarianId"
									name="librarian_input" value="${list.email}">
							</div>
						</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script>
			document.addEventListener('DOMContentLoaded', () => {

			    const getSort = ({ target }) => {
			        const order = (target.dataset.order = -(target.dataset.order || -1));
			        const index = [...target.parentNode.cells].indexOf(target);
			        const collator = new Intl.Collator(['en', 'ua'], { numeric: true });
			        const comparator = (index, order) => (a, b) => order * collator.compare(
			            a.children[index].innerHTML,
			            b.children[index].innerHTML
			        );
			        
			        for(const tBody of target.closest('table').tBodies)
			            tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			        for(const cell of target.parentNode.cells)
			            cell.classList.toggle('sorted', cell === target);
			    };
			    
			    document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
			    
			});</script>

	<div style="display: flex; flex-direction: row;">
		<c:forEach var="i" begin="1" end="${numberOfPages}">
			<c:if test="${i eq currentPage}">
				<button style="background: bisque;">${i}</button>
			</c:if>
			<c:if test="${!(i eq currentPage)}">
				<form action="ControllerAdmin" class="unban" method="post">
					<input type="hidden" name="command" value="findUsersLimit">
					<input type="hidden" name="page" value="${i}">
					<button>${i}</button>
				</form>
			</c:if>

		</c:forEach>
	</div>

</body>

</html>