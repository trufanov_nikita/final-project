<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<head>
<meta charset="UTF-8">
<title>My test</title>
<c:if test="condition"></c:if>
<link rel="stylesheet" href="./index.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap"
	rel="stylesheet">
<link href="http://fonts.cdnfonts.com/css/roboto" rel="stylesheet">
<script src="./user.js" defer></script>
</head>

<body class="body">
	<header class="header">
		<div class="menu">

			<div class="options">
				<c:if test="${sessionScope.lang eq 'en'}">

					<form action="ChangeLanguage" method="post">
						<button>UA</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_en.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>


				</c:if>
				<c:if test="${sessionScope.lang eq 'ua'}">
					<form action="ChangeLanguage" method="post">
						<button>EN</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_ua.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>

				</c:if>


			</div>

			<div class="library">
				<span class="library_text"><a href="index.jsp"><fmt:message
							key="label.homepage" /></a></span> <img src="./assets/library_image.png"
					alt="library_image" class="library_image">
			</div>



			<div class="account">
				<a href="Profile" class="account_button">
						<img src="./assets/profile_image.png" alt="profile_logo"
							class="profile_image" width="30" height="30">
				</a>
			</div>
		</div>
	</header>

	<main class="main">
	<div class="main_wrapper">

		<div class="order_wrapper">
			<form action="ControllerGuest" class="search" method="post">
				<input type="hidden" name="command" value="getCatalog"> <input
					type="hidden" name="page" value="1">
				<div class="submit_search_button">
					<button type="submit" class="submit">
						<div class="submit_wrapper">
							<span><fmt:message key="label.catalog" /></span>
						</div>
					</button>
				</div>
			</form>
			<form action="ControllerGuest" class="search" method="post">
				<input type="hidden" name="command" value="userSubscriptions">
				<input type="hidden" name="page" value="1">
				<div class="submit_search_button">
					<button type="submit" class="submit">
						<div class="submit_wrapper">
							<span><fmt:message key="label.subs" /></span>
						</div>
					</button>
				</div>
			</form>
		</div>
		<div class="search_wrapper_main" id="search_id">
			<div class="searchbar_wrapper">
				<label for="search" class="label"> <span class="label"><fmt:message
							key="label.search_under" /></span>
				</label>
				<form action="ControllerGuest" class="search" method="post">
					<input type="hidden" name="command" value="search">
					<div class="submit_search_button">
						<button type="submit" class="submit">
							<div class="submit_wrapper">
								<img src="assets/submit_button.png" alt="submit"
									class="submit_image">
							</div>
						</button>
						<input type="text" class="search_input" id="searchid"
							name="search_input"
							placeholder="<fmt:message key="label.search" />">
						<div class="radio_input">
							<input type="radio" id="contactChoice1" name="radio_input"
								value="book" checked> <label for="contactChoice1">Book</label>

							<input type="radio" id="contactChoice2" name="radio_input"
								value="author"> <label for="contactChoice2">Author</label>
						</div>
					</div>
				</form>
			</div>
			<div class="under_search">
				<div class="under_search_bar">
					<form action="ControllerGuest" method="post">
						<input type="hidden" name="command" value="authors">
						<button>
							<span class="authors_search"><fmt:message
									key="label.authors" /></span>
						</button>
					</form>
					<form action="ControllerGuest" method="post">
						<input type="hidden" name="command" value="books">
						<button>
							<span class="authors_search"><fmt:message
									key="label.books" /></span>
						</button>
					</form>
					<form action="ControllerGuest" method="post">
						<input type="hidden" name="command" value="mostPopular">
						<button>
							<span class="authors_search"><fmt:message
									key="label.popular" /></span>
						</button>
					</form>
				</div>
				<div class="credentials">
						<div class="credential">

							<div class="credential_text">
								<span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span>
							</div>
						</div>
						<div class="credential">

							<div class="credential_text">
								<span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span>
							</div>
						</div>
						<div class="credential">

							<div class="credential_text">
								<span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>

								</span> <span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span>
							</div>
						</div>
						<div class="credential">

							<div class="credential_text">
								<span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span> <span class="credential_text_inner"> <a href=""
									class="credential_link" id="credential_id">Some text</a>
								</span>
							</div>
						</div>
					</div>
			</div>
		
		</div>
		</div>
	</main>
</body>