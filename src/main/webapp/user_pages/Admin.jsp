<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="index.css">


<meta charset="UTF-8">
<title>Admin</title>
</head>
<body class="body">
	<header class="header">
		<div class="menu">

			<div class="options">
				<c:if test="${sessionScope.lang eq 'en'}">

					<form action="ChangeLanguage" method="post">
						<button>UA</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_en.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>


				</c:if>
				<c:if test="${sessionScope.lang eq 'ua'}">
					<form action="ChangeLanguage" method="post">
						<button>EN</button>
					</form>
					<div class="language_image_wrapper">
						<img src="./assets/language_image_ua.png" id="image_id"
							alt="language_image" class="language_image" width="30"
							height="30">
					</div>

				</c:if>


			</div>
			<div class="library">
				<span class="library_text"><a href="index.jsp"><fmt:message
							key="label.homepage" /></a></span> <img src="assets/library_image.png"
					alt="library_image" class="library_image">
			</div>
			<div class="account">
				<a href="Profile" class="account_button">
					<div class="image">

						<img src="./assets/profile_image.png" alt="profile_logo"
							class="profile_image" width="30" height="30">

					</div>
				</a>
			</div>


		</div>
	</header>
	<main class="main">
		<div class="main_wrapper">
			<div>
				<span><fmt:message key="welcome" /> ${name}</span>
			</div>
			<div class="upper_manual">
				
				<form action="ControllerAdmin" class="find_admin" method="post">
					<input type="hidden" name="command" value="findUsersLimit">
					<input type="hidden" name="page" value="1">
					<button><span><fmt:message key="admin.users" /></span></button>
				</form>
				<form action="ControllerAdmin" class="find_admin" method="post">
					<input type="hidden" name="command" value="findBooksLimit">
					<input type="hidden" name="page" value="1">
					<button><span><fmt:message key="admin.books" /></span></button>
				</form>
			</div>

			<div class="manual_admin">

					<div class="manuals_wrapper">
						<form action="ControllerAdmin" class="ban" method="post">
							<input type="hidden" name="command" value="ban">
							<div class="ban_search_button">
								<button type="submit" class="submit">
									<div class="ban_wrapper">
										<span><fmt:message
									key="admin.ban" /></span>
									</div>
								</button>
								<input type="text" class="ban_input" id="banid" name="ban_input"
									placeholder="Write user email">
							</div>
						</form>
					</div>
				<div class="unban_wrapper_main" id="unban_id">
					<div class="manuals_wrapper">
						<form action="ControllerAdmin" class="unban" method="post">
							<input type="hidden" name="command" value="unBan">
							<div class="unban_search_button">
								<button type="submit" class="submit">
									<div class="unban_wrapper">
										<span><fmt:message
									key="admin.unban" /></span>
									</div>
								</button>
								<input type="text" class="unban_input" id="unbanid"
									name="unban_input" placeholder="Write user email">
							</div>
						</form>
						</div>
					<div class="librarian_wrapper">
						<div class="manuals_wrapper">
							<form action="ControllerAdmin" class="add_librarian"
								method="post">
								<input type="hidden" name="command" value="addLibrarian">
								<div class="librarian_add_button">
									<button type="submit" class="submit">
										<div class="librarian_wrapper">
											<span><fmt:message key="admin.create" /></span>
										</div>
									</button>
									<input type="text" class="librarian_input" id="librarianId"
										name="librarian_input" placeholder="Write librarian email">
								</div>
							</form>
						</div>
						<div class="manuals_wrapper">
						<form action="ControllerAdmin" class="remove_librarian"
							method="post">
							<input type="hidden" name="command" value="removeLibrarian">
							<div class="librarian_remove_button">
								<button type="submit" class="submit">
									<span><fmt:message key="admin.delete" /></span>
								</button>
								<input type="text" class="librarian_input" id="librarianId"
									name="librarian_input" placeholder="Write librarian email">
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>

			<div class="book_add_wrapper">
				<div class="add_book_inputs">
					<form action="ControllerAdmin" class="book_add_form" method="post">
						<input type="hidden" name="command" value="addBook"> <input
							type="text" class="book_add_input" id="book_name_id"
							name="book_name" placeholder="Write book name"> <input
							type="number" min="0" class="book_add_input" id="book_number_id"
							name="book_number" placeholder="Write books number"> <input
							type="text" class="book_add_input" id="book_author_name_id"
							name="book_author_name" placeholder="Write author name">
						<input type="text" class="book_add_input"
							id="book_author_surname_id" name="book_author_surname"
							placeholder="Write author surname">
						<div class="submit_book">
							<button class="submit_button">
								<span class="submit_button_text"><fmt:message
										key="add_book" /></span>
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		</div>
	</main>
</body>
</html>