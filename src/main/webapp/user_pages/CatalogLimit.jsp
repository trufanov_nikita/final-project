<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<html>

<head>
<link rel="stylesheet" href="user_css/table.css">
<title>Catalog</title>
</head>
<body>
<div class="top_navbar" style="display: flex; flex-direction: column;">
	<form action="ControllerGuest" class="unban" method="post"
		style="display: flex; justify-content: center;">
		<input type="hidden" name="command" value="userInterface">
		<button>User Interface</button>
	</form>
	<div class="current_page">
		<span><fmt:message key="current_page" /> ${currentPage}</span>
	</div>
</div>
<table id="2" class="table_sort">
	<thead>
		<tr>
			<!--When a header is clicked, run the sortTable function, with a parameter, 0 for sorting by names, 1 for sorting by country:-->

			<th><fmt:message key="table.book.name" /></th>
			<th><fmt:message key="table.author.name" /></th>
			<th><fmt:message key="table.author.surname" /></th>
			<th><fmt:message key="table.book.number" /></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${bookList}" var="book">
			<form action="ControllerGuest" class="unban" method="post">
				<input type="hidden" name="command" value="clientOrder"> <input
					type="hidden" value="${book.id}" READONLY name="book_id">
				<tr>
					<td>${book.name}<input type="hidden" value="${book.name}"
						READONLY name="book_name"></td>
					<td>${book.author.getName()}<input type="hidden"
						value="${book.author.getName()}" READONLY name="book_author_name">
					</td>
					<td>${book.author.getSurname()}<input type="hidden"
						value="${book.author.getSurname()}" READONLY
						name="book_author_surname"></td>
					<td>${book.bookNumber}<input type="hidden"
						value="${book.bookNumber}" READONLY name="book_number"></td>
					<td><button class="button">
							<fmt:message key="table.order" />
						</button></td>
				</tr>
			</form>
		</c:forEach>
	</tbody>
</table>
<script>
			document.addEventListener('DOMContentLoaded', () => {

			    const getSort = ({ target }) => {
			        const order = (target.dataset.order = -(target.dataset.order || -1));
			        const index = [...target.parentNode.cells].indexOf(target);
			        const collator = new Intl.Collator(['en', 'ua'], { numeric: true });
			        const comparator = (index, order) => (a, b) => order * collator.compare(
			            a.children[index].innerHTML,
			            b.children[index].innerHTML
			        );
			        
			        for(const tBody of target.closest('table').tBodies)
			            tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			        for(const cell of target.parentNode.cells)
			            cell.classList.toggle('sorted', cell === target);
			    };
			    
			    document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
			    
			});</script>
<div style="display: flex; flex-direction: row;">
	<c:forEach var="i" begin="1" end="${numberOfPages}">
		<c:if test="${i eq currentPage}">
			<button style="background: bisque;">${i}</button>
		</c:if>
		<c:if test="${!(i eq currentPage)}">
			<form action="ControllerGuest" class="unban" method="post">
				<input type="hidden" name="command" value="getCatalog"> <input
					type="hidden" name="page" value="${i}">
				<button>${i}</button>
			</form>
		</c:if>

	</c:forEach>
</div>

</body>

</html>