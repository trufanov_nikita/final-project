<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="./index.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>EditPage</title>
</head>
<body class="body">
	<main class="main">
		<div class="main_wrapper"
			style="height: 90vh; align-items: center; align-content: center; display: flex; padding-top: 8vh;">
			<span class="edit_label"><fmt:message key="edit.change" /></span>
			<form action="ControllerAdmin" class="book_add_form" method="post"
				style="display: flex; flex-direction: column;">
				<div class="edit" style="display: flex; padding: 2vh;">
					<div class="chosen" style="display: flex; flex-direction: column;">
						<input type="hidden" name="command" value="editBook"> <input
							type="hidden" min="0" value="${book.id}" READONLY name="book_id">
						<input type="text" value="${book.name}" READONLY name="book_name">
						<input type="number" value="${book.bookNumber}" READONLY
							name="book_number"> <input type="text"
							value="${book.author.getName()}" READONLY name="book_author_name">
						<input type="text" value="${book.author.getSurname()}" READONLY
							name="book_author_surname">
					</div>
					<div class="inputs">

						<div class="edit_wrapper" style="padding-left: 8vh;">
							<input class="book_add_input" id="book_name_id"
								name="book_name_change" placeholder="Changed book name">
							<input type="number" min="0" class="book_add_input"
								id="book_number_id" name="book_number_change"
								placeholder="Changed books number"> <input
								class="book_add_input" id="book_author_name_id"
								name="book_author_name_change" placeholder="Changed author name">
							<input class="book_add_input" id="book_author_surname_id"
								name="book_author_surname_change"
								placeholder="Changed author surname">
							<div class="submit_book" style="display: flex;"></div>
						</div>



					</div>
				</div>
				<button class="submit_button">
					<span class="submit_button_text"><fmt:message
							key="edit.submit" /></span>
				</button>
			</form>

		</div>
	</main>
</body>
</html>