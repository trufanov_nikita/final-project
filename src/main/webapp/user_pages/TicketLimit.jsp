<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<html>

<head>
<link rel="stylesheet" href="user_css/table.css">
<title>Books</title>
</head>
<form action="ControllerLibrarian" class="unban" method="post"
	style="display: flex; justify-content: center;">
	<input type="hidden" name="command" value="librarianInterface">
	<button>Librarian Interface</button>
</form>
<span><fmt:message key="current_page" /> ${currentPage}</span>

<table id="2" class="table_sort">
	<thead>
		<tr>
			<th><fmt:message key="table.ticket" /></th>
			<th><fmt:message key="table.user.name" /></th>
			<th><fmt:message key="table.user.surname" /></th>
			<th><fmt:message key="table.user.email" /></th>
			<th><fmt:message key="table.book.name" /></th>
			<th><fmt:message key="table.author.name" /></th>
			<th><fmt:message key="table.author.surname" /></th>
			<th><fmt:message key="table.status" /></th>
			<th><fmt:message key="table.date" /></th>
			<th><fmt:message key="table.fee" /></th>
			<th><fmt:message key="table.change_status" /></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list_subs}" var="subs">
			<form action=ControllerLibrarian class="unban" method="post">
				<input type="hidden" name="command" value="editSubscription">
				<input type="hidden" value="${subs.id}" READONLY name="subs_id">
				<input type="hidden" value="${subs.book.getId()}" READONLY
					name="book_id"> <input type="hidden" value="${subs.status}"
					READONLY name="sub_status">
				<tr>
					<td>${subs.id}</td>
					<td>${subs.user.getName()}<input type="hidden"
						value="${book.author.getName()}" READONLY name="book_author_name">
					</td>
					<td>${subs.user.getSurname()}<input type="hidden"
						value="${book.author.getSurname()}" READONLY
						name="book_author_surname"></td>
					<td>${subs.user.getEmail()}<input type="hidden"
						value="${book.author.getSurname()}" READONLY
						name="book_author_surname"></td>
					<td>${subs.book.getName()}<input type="hidden"
						value="${book.bookNumber}" READONLY name="book_number"></td>
					<td>${subs.book.author.getName()}<input type="hidden"
						value="${book.bookNumber}" READONLY name="book_number"></td>
					<td>${subs.book.author.getSurname()}<input type="hidden"
						value="${book.bookNumber}" READONLY name=""></td>
					<c:if test="${subs.status eq 'DECLINED'}">
						<td class="status"
							style="background-color: #f96d6d; color: white;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'ENDED'}">
						<td class="status"
							style="background-color: #6767fb; color: white;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'APROVED'}">
						<td class="status" style="background-color: #54dfeb;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'NEW'}">
						<td class="status">${subs.status}</td>
					</c:if>
					<td>${subs.date}<input type="hidden"
						value="${book.bookNumber}" READONLY name="book_number"></td>
					<td>${subs.fee}<input type="hidden" value="${book.bookNumber}"
						READONLY name="book_number"></td>


					<td><c:set var="status" scope="page" value="${subs.status}" />

						<%
						String status = String.valueOf(pageContext.getAttribute("status"));
						switch (status) {
							case "NEW" :
						%> <select name="status" id="status">
							<option value="DECLINED"><fmt:message
									key="status.declined" /></option>
							<option value="APROVED"><fmt:message
									key="status.aproved" /></option>

					</select> <%
 break;
 case "APROVED" :
 %> <select name="status" id="status">
							<option value="ENDED"><fmt:message key="status.ended" /></option>
					</select> <%
 break;
 default :

 }
 %></td>
					<td><button class="button">
							<fmt:message key="table.change_status" />
						</button></td>
				</tr>
			</form>
		</c:forEach>
	</tbody>
</table>
<script>
			document.addEventListener('DOMContentLoaded', () => {

			    const getSort = ({ target }) => {
			        const order = (target.dataset.order = -(target.dataset.order || -1));
			        const index = [...target.parentNode.cells].indexOf(target);
			        const collator = new Intl.Collator(['en', 'ua'], { numeric: true });
			        const comparator = (index, order) => (a, b) => order * collator.compare(
			            a.children[index].innerHTML,
			            b.children[index].innerHTML
			        );
			        
			        for(const tBody of target.closest('table').tBodies)
			            tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			        for(const cell of target.parentNode.cells)
			            cell.classList.toggle('sorted', cell === target);
			    };
			    
			    document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
			    
			});</script>
<div style="display: flex; flex-direction: row;">
	<c:forEach var="i" begin="1" end="${numberOfPages}">
		<c:if test="${i eq currentPage}">
			<button style="background: bisque;">${i}</button>
		</c:if>
		<c:if test="${!(i eq currentPage)}">
			<form action="ControllerLibrarian" class="unban" method="post">
				<input type="hidden" name="command" value="subsLimit"> <input
					type="hidden" name="page" value="${i}">
				<button>${i}</button>
			</form>
		</c:if>

	</c:forEach>
</div>

</body>

</html>