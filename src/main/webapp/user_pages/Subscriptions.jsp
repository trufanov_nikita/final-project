<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="user_css/table.css">

</head>
<form action="ControllerGuest" class="unban" method="post"
	style="display: flex; justify-content: center;">
	<input type="hidden" name="command" value="userInterface">
	<button>User Interface</button>
</form>
<body>
	<table id="2" class="table_sort">
		<thead>
			<tr>
				<!--When a header is clicked, run the sortTable function, with a parameter, 0 for sorting by names, 1 for sorting by country:-->

				<th><fmt:message key="table.ticket" /></th>
				<th><fmt:message key="table.user.name" /></th>
				<th><fmt:message key="table.user.surname" /></th>
				<th><fmt:message key="table.user.email" /></th>
				<th><fmt:message key="table.book.name" /></th>
				<th><fmt:message key="table.author.name" /></th>
				<th><fmt:message key="table.author.surname" /></th>
				<th><fmt:message key="table.status" /></th>
				<th><fmt:message key="table.date" /></th>
				<th><fmt:message key="table.fee" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list_subs}" var="subs">
				<tr>
					<td>${subs.id}</td>
					<td>${subs.user.getName()}</td>
					<td>${subs.user.getSurname()}</td>
					<td>${subs.user.getEmail()}</td>
					<td>${subs.book.getName()}</td>
					<td>${subs.book.author.getName()}</td>
					<td>${subs.book.author.getSurname()}</td>
					<c:if test="${subs.status eq 'DECLINED'}">
						<td class="status"
							style="background-color: #f96d6d; color: white;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'ENDED'}">
						<td class="status"
							style="background-color: #6767fb; color: white;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'APROVED'}">
						<td class="status" style="background-color: #54dfeb;">${subs.status}</td>
					</c:if>
					<c:if test="${subs.status eq 'NEW'}">
						<td class="status">${subs.status}</td>
					</c:if>
					<td>${subs.date}</td>
					<td class="amount">${subs.fee}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script>
			document.addEventListener('DOMContentLoaded', () => {

			    const getSort = ({ target }) => {
			        const order = (target.dataset.order = -(target.dataset.order || -1));
			        const index = [...target.parentNode.cells].indexOf(target);
			        const collator = new Intl.Collator(['en', 'ua'], { numeric: true });
			        const comparator = (index, order) => (a, b) => order * collator.compare(
			            a.children[index].innerHTML,
			            b.children[index].innerHTML
			        );
			        
			        for(const tBody of target.closest('table').tBodies)
			            tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			        for(const cell of target.parentNode.cells)
			            cell.classList.toggle('sorted', cell === target);
			    };
			    
			    document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
			    
			});
</script>


</body>
</html>