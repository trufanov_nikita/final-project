<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="main_page_css/jsp_authors.css" rel="stylesheet"
	type="text/css">

<title>Authors</title>
</head>
<body>
	<div class="first">
		<c:forEach items="${authors_map}" var="authors">
			<div class="authors">
				<div class="author_list">
					<span class="text">${authors.key.name}</span> <span class="text">${authors.key.surname}</span>
					<span class="text">Books: </span>
				</div>
				<c:forEach items="${authors.value}" var="books">
					<div class="book_list">
						<span class="text">${books.name}</span>
						<span class="text">${books.bookNumber}</span>
					</div>
				</c:forEach>
			</div>
		</c:forEach>
	</div>
</body>
</html>