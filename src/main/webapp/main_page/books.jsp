<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />

<fmt:setBundle basename="messages" />
<html>
<head>
<link href="main_page_css/jsp_books.css" rel="stylesheet"
	type="text/css">
<title>${search}</title>
</head>
<body>
	<div class="first">
		<c:forEach items="${list_books_first}" var="list">
			<form action="ControllerGuest" class="unban" method="post">
				<input type="hidden" name="command" value="clientOrder"> <input
					type="hidden" value="${list.id}" READONLY name="book_id"> <input
					type="hidden" value="${list.name}" READONLY name="book_name">
				<input type="hidden" value="${list.author.getName()}" READONLY
					name="book_author_name"> <input type="hidden"
					value="${list.author.getSurname()}" READONLY
					name="book_author_surname"> <input type="hidden"
					value="${list.bookNumber}" READONLY name="book_number">
				<div class="book">
					<span class="label">${list.name}</span>
					<div class="book_character">
						<span class="text">Visits: ${list.visits}</span> <span
							class="text">Author Name ${list.author.name}</span> <span
							class="text">Author Surname ${list.author.surname}</span>
						<button class="button">
							<fmt:message key="table.order" />
						</button>
					</div>
				</div>
			</form>
		</c:forEach>
	</div>
	<div class="first">
		<c:forEach items="${list_books_second}" var="list">
			<form action="ControllerGuest" class="unban" method="post">
				<input type="hidden" name="command" value="clientOrder"> <input
					type="hidden" value="${list.id}" READONLY name="book_id"> <input
					type="hidden" value="${list.name}" READONLY name="book_name">
				<input type="hidden" value="${list.author.getName()}" READONLY
					name="book_author_name"> <input type="hidden"
					value="${list.author.getSurname()}" READONLY
					name="book_author_surname"> <input type="hidden"
					value="${list.bookNumber}" READONLY name="book_number">
				<div class="book">
					<span class="label">${list.name}</span>
					<div class="book_character">
						<span class="text">Visits: ${list.visits}</span> <span
							class="text">Author Name ${list.author.name}</span> <span
							class="text">Author Surname ${list.author.surname}</span>
						<button class="button">
							<fmt:message key="table.order" />
						</button>
					</div>
				</div>
			</form>
		</c:forEach>
	</div>
</body>
</html>