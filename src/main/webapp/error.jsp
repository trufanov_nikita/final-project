
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.lang}"/>

<fmt:setBundle basename="messages" />
<html>
<body>
	<div style="text-align: center;">
		<fmt:message key="error.line" />
		<h3>${requestScope.errorMessage}</h3>
		<a href="javascript:history.back()"><fmt:message key="error.return" /></a>
	</div>
</body>
</html>