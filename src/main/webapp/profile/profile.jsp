<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="messages" />
<head>
<title>Profile</title>
<link rel="stylesheet" href="profile/profile.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap"
	rel="stylesheet">
<link href="http://fonts.cdnfonts.com/css/roboto" rel="stylesheet">
</head>
<body class="body">
	<div class="body_wrapper">
		<nav class="header">
			<div class="header_wrapper">
				<div class="header_text">
					<span class="header_text_inner"> <fmt:message key="profile" />
					</span>
				</div>
				<div class="header_text">

					<a href="Redirect" class="return_link"><fmt:message
							key="profile.home" /></a>
				</div>
				<div class="header_language">
					<div class="options">
						<div class="options">
							<c:if test="${sessionScope.lang eq 'en'}">

								<form action="ChangeLanguage" method="post">
									<button>UA</button>
								</form>
								<div class="language_image_wrapper">
									<img src="./assets/language_image_en.png" id="image_id"
										alt="language_image" class="language_image" width="30"
										height="30">
								</div>


							</c:if>
							<c:if test="${sessionScope.lang eq 'ua'}">
								<form action="ChangeLanguage" method="post">
									<button>EN</button>
								</form>
								<div class="language_image_wrapper">
									<img src="./assets/language_image_ua.png" id="image_id"
										alt="language_image" class="language_image" width="30"
										height="30">
								</div>

							</c:if>


						</div>
					</div>
				</div>
			</div>
		</nav>
		<main class="main">
			<div class="main_wrapper">

				<div class="profile_top_wrapper">
					<div class="image_wrapper">
						<img src="assets/profile_image.png" alt="" class="profile_image"
							style="border-radius: 22px;">
					</div>
					<div class="names">

						<div class="name_wrapper">
							<span class="label"><fmt:message key="label.name" /></span> <span
								class="name">${user.name}</span>
						</div>
						<div class="surname_wrapper">
							<span class="label"><fmt:message key="label.surname" /></span> <span
								class="surname">${user.surname}</span>
						</div>
						<div class="position_wrapper">
							<span class="label"><fmt:message key="table.user.pos" /></span>
							<span class="position">${position}</span>
						</div>
					</div>
				</div>
				<div class="statistics">
					<div class="fine_order">
						<div class="statistics_numbers">
							<span class="stat"><fmt:message key="profile.stat.orders" /></span>
							<span class="orders">${count}</span>
						</div>
						<div class="statistics_numbers">
							<span class="stat"><fmt:message key="profile.stat.active" /></span>
							<span class="current">${countOnHands}</span>
						</div>
						<div class="statistics_numbers">
							<span class="stat"><fmt:message key="profile.stat.fee" /></span>
							<span class="fine">${fee}</span>
						</div>

					</div>
					<div class="favorite_wrapper">
						<div class="favorite">
							<span class="stat"><fmt:message key="profile.stat.book" /></span>
							<span class="book">${sub.book.name}</span>
						</div>
						<div class="favorite">
							<span class="stat"><fmt:message key="profile.stat.author" /></span>
							<span class="book">${author.name} ${author.surname}</span>
						</div>
					</div>
				</div>
			</div>
		</main>
		<bottom class="bottom">
		<div class="bottom_wrapper"></div>
		</bottom>
	</div>
</body>