package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import controller.PathContainer;
import exception.WebException;

public class LibrarianInterfaceTest {

	public static LibrarianInterface test = new LibrarianInterface();
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@After
	public void tearDown() throws Exception {
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;
	
	@Mock
	HttpSession session;
 
	@Test
	public void testCorrect() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		String res = "";
		res = test.execute(request, response);

		assertEquals(PathContainer.LIBRARIAN, res);
	}
	
	@Test
	public void testNull() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		 WebException thrown = Assert.assertThrows(WebException.class, () -> {
			 test.execute(request, response);
		 });
		 assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}
	@Test
	public void testWrongPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		 WebException thrown = Assert.assertThrows(WebException.class, () -> {
			 test.execute(request, response);
		 });
		 assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}

}

