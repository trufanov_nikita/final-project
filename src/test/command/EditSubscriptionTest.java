package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class EditSubscriptionTest {
	private static EditSubscription test = new EditSubscription();
	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	@Test
	public void testNullPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}
	@Test
	public void testWrongPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}
	
	@Test
	public void testWrongStatus() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn("3");
		when(request.getParameter("book_id")).thenReturn("3");
		when(request.getParameter("status")).thenReturn("APRVD");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong status modifier", thrown.getMessage());
	}
	@Test
	public void testNullStatus() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn("3");
		when(request.getParameter("book_id")).thenReturn("3");
		when(request.getParameter("status")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong inputs", thrown.getMessage());
	}
	@Test
	public void testBlankStatus() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn("3");
		when(request.getParameter("status")).thenReturn(" ");
		when(request.getParameter("book_id")).thenReturn(" ");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong inputs", thrown.getMessage());
	}
	@Test
	public void testWrongSubsId() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn("3d");
		when(request.getParameter("book_id")).thenReturn("3d");
		when(request.getParameter("status")).thenReturn("APRVD");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong inputs", thrown.getMessage());
	}
	@Test
	public void testNullSubsId() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn(null);
		when(request.getParameter("book_id")).thenReturn("3");
		when(request.getParameter("status")).thenReturn("APRVD");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong inputs", thrown.getMessage());
	}
	@Test
	public void testBlankSubsId() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		when(request.getParameter("subs_id")).thenReturn(" ");
		when(request.getParameter("book_id")).thenReturn("3");
		when(request.getParameter("status")).thenReturn("APRVD");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong inputs", thrown.getMessage());
	}

	@Test
	public void testCorrectAprovedOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(2);
			when(request.getParameter("subs_id")).thenReturn("3");
			when(request.getParameter("status")).thenReturn("APROVED");
			when(request.getParameter("book_id")).thenReturn("3");
			doNothing().when(db).editSubStatus(3, "APROVED", 3);
			when(db.checkSubStatus(3, "APROVED")).thenReturn(true);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.SUCCESS, rs);
		}
	}
	@Test
	public void testCorrectDeclinedOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(2);
			when(request.getParameter("book_id")).thenReturn("3");
			when(request.getParameter("subs_id")).thenReturn("3");
			when(request.getParameter("status")).thenReturn("DECLINED");
			when(db.checkSubStatus(3, "DECLINED")).thenReturn(true);
			doNothing().when(db).editSubStatus(3, "DECLINED", 3);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.SUCCESS, rs);
		}
	}
	@Test
	public void testSubStatusWrong() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(2);
			when(request.getParameter("book_id")).thenReturn("3");
			when(request.getParameter("subs_id")).thenReturn("3");
			when(request.getParameter("status")).thenReturn("DECLINED");
			when(db.checkSubStatus(3, "DECLINED")).thenReturn(false);
			doNothing().when(db).editSubStatus(3, "DECLINED", 3);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Cant modify order with chosen status", thrown.getMessage());
		}
	}
	@Test
	public void testCorrectEndedOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(2);
			when(request.getParameter("subs_id")).thenReturn("3");
			when(request.getParameter("book_id")).thenReturn("3");
			when(request.getParameter("status")).thenReturn("ENDED");
			when(db.checkSubStatus(3, "ENDED")).thenReturn(true);
			doNothing().when(db).editSubStatus(3, "ENDED", 3);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.SUCCESS, rs);
		}
	}

}
