package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class AddLibrarianTest {
	AddLibrarian test = new AddLibrarian();
	
	final static String EMAIL = "email";
	final static String NAME = "name";
	final static String SURNAME = "surname";
	final static int POS = 2;
	
	final static User USER = User.createUser(NAME, SURNAME, EMAIL, POS);
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@After
	public void tearDown() throws Exception {

	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;
	@Mock
	HttpSession session;
	
	@Mock
	DBManager db;
	
	@Captor
	ArgumentCaptor<String> emailCapture;
	
	@Test
	public void testPosNull() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		 WebException thrown = Assert.assertThrows(WebException.class, () -> {
			 test.execute(request, response);
		 });
		 assertEquals("You are not loged in", thrown.getMessage());
	}
	@Test
	public void testPosNotCorrect() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		 WebException thrown = Assert.assertThrows(WebException.class, () -> {
			 test.execute(request, response);
		 });
		 assertEquals("You dont have permission for this operation", thrown.getMessage());
	}
	@Test
	public void testEmailNull() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("librarian_input")).thenReturn(null);
		 WebException thrown = Assert.assertThrows(WebException.class, () -> {
			 test.execute(request, response);
		 });
		 assertEquals("Email cant be null", thrown.getMessage());
	}
	@Test
	public void testEmailWrong() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("librarian_input")).thenReturn(EMAIL);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(null);
			 WebException thrown = Assert.assertThrows(WebException.class, () -> {
				 test.execute(request, response);
			 });
			 List<String> list = emailCapture.getAllValues();
			assertEquals(EMAIL, emailCapture.getValue());
			 assertEquals("We dont find this user", thrown.getMessage());
			
		}
		
		
	}
	@Test
	public void testCorrectOperationt() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("librarian_input")).thenReturn(EMAIL);
		String res = "";
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			
			doNothing().when(db).createLibrarian(emailCapture.capture());
			
			res = test.execute(request, response);
			List<String> list = emailCapture.getAllValues();
			assertEquals(EMAIL, list.get(0));
			assertEquals( EMAIL, list.get(1));
			assertEquals(EMAIL, emailCapture.getValue());
			assertEquals(PathContainer.SUCCESS,res);
			
		}
	}
}
