package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class GetUsersTest {
	private static GetUsers test = new GetUsers();
	private static final List<User> USER = new ArrayList<>();
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	@Test
	public void testNullPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfUsers()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfUsers()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(" ");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testWrongPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfUsers()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("5asd");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testCorrect() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		USER.add(User.createUser(1, "name", "surname","email", 1, "No"));
		USER.add(User.createUser(2, "name", "surname","email2", 1, "No"));
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfUsers()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("2");
			when(db.findLimitUsers(10, 10)).thenReturn(USER);
			String res = test.execute(request, response);
			verify(request).setAttribute(eq("numberOfPages"), eq(2));
			verify(request).setAttribute(eq("currentPage"), eq(2));
			verify(request).setAttribute(eq("list_users"), eq(USER));
			assertEquals(PathContainer.ADMIN_USERS, res);
			
		}
	}
	@Test
	public void testWrongPos() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfBooks()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("5");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}
	@Test
	public void testNullPos() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfUsers()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("5");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}

}
