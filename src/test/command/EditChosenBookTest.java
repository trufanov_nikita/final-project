package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import exception.WebException;

public class EditChosenBookTest {
	private static final Author AUTHOR = Author.createAuthor("name", "surname");
	private static final Book BOOK = Book.createBook(1, "NAME", AUTHOR, 5);
	private static EditChosenBook test = new EditChosenBook();
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testWrongPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}
	
	@Test
	public void testNullPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}
	@Test
	public void testNullInputsPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_id")).thenReturn(null);
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn("test");
		when(request.getParameter("book_author_name")).thenReturn("test");
		when(request.getParameter("book_author_surname")).thenReturn("test");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wronk inputs", thrown.getMessage());
	}
	@Test
	public void testBlankInputsPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_id")).thenReturn(null);
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn("test");
		when(request.getParameter("book_author_name")).thenReturn("test");
		when(request.getParameter("book_author_surname")).thenReturn("test");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wronk inputs", thrown.getMessage());
	}
	
	@Test
	public void testCorrectOperation() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_id")).thenReturn("1");
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn(BOOK.getName());
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
		when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
		doNothing().when(request).setAttribute("book", BOOK);
		String rs = test.execute(request, response);
		assertEquals(rs, PathContainer.EDIT);

	}

}
