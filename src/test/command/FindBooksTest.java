package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class FindBooksTest {
	private static FindBooks test = new FindBooks();
	private static final List<Book> BOOK = new ArrayList<>();
	private static final List<Book> BOOK_FIRST = new ArrayList<>();
	private static final List<Book> BOOK_SECOND = new ArrayList<>();
	private static final Author AUTHOR = Author.createAuthor(1, "Nmae", "Surname");

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	
	@Captor
	ArgumentCaptor<List<Book>> bookCapture;

	@Test
	public void test() throws WebException {
		BOOK.add(Book.createBook(1,"Book", AUTHOR, 5));
		BOOK.add(Book.createBook(2,"Book2", AUTHOR, 5));
		BOOK_FIRST.add(Book.createBook(1,"Book", AUTHOR, 5));
		BOOK_SECOND.add(Book.createBook(2,"Book2", AUTHOR, 5));
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findLimitBooks(0,10)).thenReturn(BOOK);
			String res = test.execute(request, response);
			verify(request).setAttribute(eq("list_books_first"), bookCapture.capture());
			verify(request).setAttribute(eq("list_books_second"), bookCapture.capture());
			List<List<Book>> books = bookCapture.getAllValues();
			assertEquals(PathContainer.BOOKS, res);
			assertEquals(BOOK_FIRST,books.get(0));
			assertEquals(BOOK_SECOND,books.get(1));
		}
	}

}
