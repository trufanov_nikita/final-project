package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class GetCatalogTest {
	private static GetCatalog test = new GetCatalog();
	private static final List<Book> BOOK = new ArrayList<>();
	private static final Author AUTHOR = Author.createAuthor(1, "Nmae", "Surname");

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;

	@Test
	public void testCorrect() throws WebException {
		BOOK.add(Book.createBook(1,"Book", AUTHOR, 5));
		BOOK.add(Book.createBook(2,"Book2", AUTHOR, 5));
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfBooks()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("2");
			when(db.findLimitBooks(10, 10)).thenReturn(BOOK);
			String res = test.execute(request, response);
			verify(request).setAttribute(eq("numberOfPages"), eq(2));
			verify(request).setAttribute(eq("currentPage"), eq(2));
			verify(request).setAttribute(eq("bookList"), eq(BOOK));
			assertEquals(PathContainer.CATALOG, res);
			
		}
	}
	@Test
	public void testNullPage() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfBooks()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankPage() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfBooks()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(" ");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testWrongPage() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfBooks()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("5asd");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}

}
