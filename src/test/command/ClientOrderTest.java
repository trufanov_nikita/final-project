package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class ClientOrderTest {
	private static ClientOrder test = new ClientOrder();
	private static final Author AUTHOR = Author.createAuthor("name", "surname");
	private static final String EMAIL = "email";
	private static final Book BOOK = Book.createBook(1, "NAME", AUTHOR, 5);
	private static final User USER = User.createUser(1, "name", EMAIL, "surname", 1, null);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;

	@Captor
	ArgumentCaptor<String> emailCapture;

	@Captor
	ArgumentCaptor<Book> bookCapture;

	@Captor
	ArgumentCaptor<Integer> intCapture;

	@Test
	public void testNullPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You are not loged in", thrown.getMessage());
	}

	@Test
	public void testNullEmail() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(null);
		when(request.getParameter("book_id")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("wrong email, try to login again", thrown.getMessage());
	}

	@Test
	public void testNullData() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn(null);
		when(request.getParameter("book_number")).thenReturn(null);
		when(request.getParameter("book_name")).thenReturn(null);
		when(request.getParameter("book_author_name")).thenReturn(null);
		when(request.getParameter("book_author_surname")).thenReturn(null);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wronk inputs", thrown.getMessage());

		}
	}

	@Test
	public void testBlankData() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn(" ");
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn("test");
		when(request.getParameter("book_author_name")).thenReturn("test");
		when(request.getParameter("book_author_surname")).thenReturn("test");
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals(EMAIL, emailCapture.getValue());
			assertEquals("Wronk inputs", thrown.getMessage());
		}

	}

	@Test
	public void testWrongNumber() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn("17");
		when(request.getParameter("book_number")).thenReturn("5d");
		when(request.getParameter("book_name")).thenReturn("test");
		when(request.getParameter("book_author_name")).thenReturn("test");
		when(request.getParameter("book_author_surname")).thenReturn("test");
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals(EMAIL, emailCapture.getValue());
			assertEquals("Wronk inputs", thrown.getMessage());
		}

	}

	@Test
	public void testCorrect() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn("1");
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn("NAME");
		when(request.getParameter("book_author_name")).thenReturn("name");
		when(request.getParameter("book_author_surname")).thenReturn("surname");
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			when(db.addTicket(bookCapture.capture(), intCapture.capture())).thenReturn(true);
			String res = test.execute(request, response);
			assertEquals(PathContainer.SUCCESS, res);
			assertEquals(EMAIL, emailCapture.getValue());
			assertEquals(BOOK, bookCapture.getValue());
		}

	}

	@Test
	public void testWrong() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn("1");
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn("NAME");
		when(request.getParameter("book_author_name")).thenReturn("name");
		when(request.getParameter("book_author_surname")).thenReturn("surname");
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			when(db.addTicket(bookCapture.capture(), intCapture.capture())).thenReturn(false);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Cant order this book", thrown.getMessage());
			assertEquals(EMAIL, emailCapture.getValue());
			assertEquals(BOOK, bookCapture.getValue());
		}

	}
	@Test
	public void testWrongBookNumber() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		when(session.getAttribute("email")).thenReturn(EMAIL);
		when(request.getParameter("book_id")).thenReturn("1");
		when(request.getParameter("book_number")).thenReturn("0");
		when(request.getParameter("book_name")).thenReturn("NAME");
		when(request.getParameter("book_author_name")).thenReturn("name");
		when(request.getParameter("book_author_surname")).thenReturn("surname");
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			when(db.addTicket(bookCapture.capture(), intCapture.capture())).thenReturn(false);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("We dont have this book now. Try later", thrown.getMessage());
		}

	}

}
