package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class LoginTest {
	private static Login test = new Login();
	private static final String EMAIL = "email";
	private static final User USER = User.createUser("name", "surname", EMAIL, 1);
	private static final User LIBRARIAN = User.createUser("name", "surname", EMAIL, 2);
	private static final User ADMIN = User.createUser("name", "surname", EMAIL, 3);
	private static final User WRONG = User.createUser("name", "surname", EMAIL, 5);
	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	@Test
	public void testNullInputs() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn(null);
			when(request.getParameter("email")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankInputs() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn(" ");
			when(request.getParameter("email")).thenReturn(" ");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testWrongPass() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(false);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong password", thrown.getMessage());
		}
	}
	@Test
	public void testUserBanned() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(true);
			when(db.findUser(EMAIL)).thenReturn(USER);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You are banned. Try to contact with librarian or admin", thrown.getMessage());
		}
	}
	@Test
	public void testUser() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(true);
			when(db.findUser(EMAIL)).thenReturn(USER);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs =test.execute(request, response);
			assertEquals(PathContainer.USER, rs);
		}
	}
	@Test
	public void testLibrarian() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(true);
			when(db.findUser(EMAIL)).thenReturn(LIBRARIAN);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs =test.execute(request, response);
			assertEquals(PathContainer.LIBRARIAN, rs);
		}
	}
	@Test
	public void testAdmin() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(true);
			when(db.findUser(EMAIL)).thenReturn(ADMIN);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs =test.execute(request, response);
			assertEquals(PathContainer.ADMIN, rs);
		}
	}
	@Test
	public void testWrongPos() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(request.getParameter("password")).thenReturn("pass");
			when(request.getParameter("email")).thenReturn(EMAIL);
			when(db.isPassCorrect(EMAIL, "pass")).thenReturn(true);
			when(db.findUser(EMAIL)).thenReturn(WRONG);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs =test.execute(request, response);
			assertEquals(PathContainer.DEFAULT, rs);
		}
	}
	
}
