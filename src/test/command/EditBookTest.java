package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class EditBookTest {
	private static EditBook test = new EditBook();
	private static final Author AUTHOR = Author.createAuthor("name", "surname");
	private static final Book BOOK = Book.createBook(1, "NAME", AUTHOR, 5);
	private static final Book BOOK_CHANGED = Book.createBook(1, "NAME", AUTHOR, 7);
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	
	@Captor
	ArgumentCaptor<Book> bookCapture;
	
	@Captor
	ArgumentCaptor<Author> authorCapture;
	@Test
	public void testWrongPos() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}
	@Test
	public void testNullPos() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}
	@Test
	public void testNullData() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn(null);
			when(request.getParameter("book_number")).thenReturn("5");
			when(request.getParameter("book_name")).thenReturn(null);
			when(request.getParameter("book_author_name")).thenReturn(null);
			when(request.getParameter("book_author_surname")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankData() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn(" ");
			when(request.getParameter("book_name")).thenReturn(" ");
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testWrongNumbers() {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn("5asd");
			when(request.getParameter("book_name")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	
	@Test
	public void testWrongOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn("5");
			when(request.getParameter("book_name")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			when(request.getParameter("book_number_change")).thenReturn("7");
			when(request.getParameter("book_name_change")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name_change")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname_change")).thenReturn(AUTHOR.getSurname());
			when(db.findAuthorId(authorCapture.capture())).thenReturn(0);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			when(db.editBook(bookCapture.capture())).thenReturn(false);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals(BOOK_CHANGED, bookCapture.getValue());
			assertEquals("Failed to edit a book", thrown.getMessage());
			authorCapture.getAllValues().forEach(x->{
				assertEquals(AUTHOR, x);
			});
		}
	}
	@Test
	public void testCorrectOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn("5");
			when(request.getParameter("book_name")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			when(request.getParameter("book_number_change")).thenReturn("7");
			when(request.getParameter("book_name_change")).thenReturn(BOOK_CHANGED.getName());
			when(request.getParameter("book_author_name_change")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname_change")).thenReturn(AUTHOR.getSurname());
			when(db.findAuthorId(authorCapture.capture())).thenReturn(0);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			when(db.editBook(bookCapture.capture())).thenReturn(true);
			String res = test.execute(request, response);
			assertEquals(PathContainer.ADMIN, res);
			assertEquals(BOOK_CHANGED, bookCapture.getValue());
			authorCapture.getAllValues().forEach(x->{
				assertEquals(AUTHOR, x);
			});
		}
	}
	@Test
	public void testCorrectBlankOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn("5");
			when(request.getParameter("book_name")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			when(request.getParameter("book_number_change")).thenReturn(" ");
			when(request.getParameter("book_name_change")).thenReturn(" ");
			when(request.getParameter("book_author_name_change")).thenReturn(" ");
			when(request.getParameter("book_author_surname_change")).thenReturn(" ");
			when(db.findAuthorId(authorCapture.capture())).thenReturn(0);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			when(db.editBook(bookCapture.capture())).thenReturn(true);
			String res = test.execute(request, response);
			assertEquals(PathContainer.ADMIN, res);
			assertEquals(BOOK, bookCapture.getValue());
			authorCapture.getAllValues().forEach(x->{
				assertEquals(AUTHOR, x);
			});
		}
	}
	@Test
	public void testCorrectNullOperation() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(request.getParameter("book_id")).thenReturn("1");
			when(request.getParameter("book_number")).thenReturn("5");
			when(request.getParameter("book_name")).thenReturn(BOOK.getName());
			when(request.getParameter("book_author_name")).thenReturn(AUTHOR.getName());
			when(request.getParameter("book_author_surname")).thenReturn(AUTHOR.getSurname());
			when(request.getParameter("book_number_change")).thenReturn(null);
			when(request.getParameter("book_name_change")).thenReturn(null);
			when(request.getParameter("book_author_name_change")).thenReturn(null);
			when(request.getParameter("book_author_surname_change")).thenReturn(null);
			when(db.findAuthorId(authorCapture.capture())).thenReturn(1);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			when(db.editBook(bookCapture.capture())).thenReturn(true);
			String res = test.execute(request, response);
			assertEquals(PathContainer.ADMIN, res);
			assertEquals(BOOK, bookCapture.getValue());
			authorCapture.getAllValues().forEach(x->{
				assertEquals(AUTHOR, x);
			});
		}
	}

}
