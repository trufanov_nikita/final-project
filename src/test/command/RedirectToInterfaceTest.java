package command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class RedirectToInterfaceTest {
	private static RedirectToInterface test = new RedirectToInterface();
	private static final String EMAIL = "email";
	private static final User USER = User.createUser("name", "surname", EMAIL, 1);
	private static final User LIBRARIAN = User.createUser("name", "surname", EMAIL, 2);
	private static final User ADMIN = User.createUser("name", "surname", EMAIL, 3);
	private static final User WRONG = User.createUser("name", "surname", EMAIL, 5);
	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	@Test
	public void testNullPos() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(null);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong position", thrown.getMessage());
		}
	}
	@Test
	public void testBlankPos() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(" ");
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong position", thrown.getMessage());
		}
	}
	@Test
	public void testWrongPos() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn("5asd");
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong position", thrown.getMessage());
		}
	}
	@Test
	public void testNullEmail() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(1);
			when(session.getAttribute("email")).thenReturn(null);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankEmail() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(1);
			when(session.getAttribute("email")).thenReturn(" ");
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBannedPos() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(1);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(1);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You are banned", thrown.getMessage());
		}
	}
	@Test
	public void testUser() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(1);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.USER, rs);
		}
	}
	@Test
	public void testLibrarian() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(2);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.LIBRARIAN, rs);
		}
	}
	@Test
	public void testAdmin() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(3);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.ADMIN, rs);
		}
	}
	@Test
	public void testDefault() throws WebException {
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(request.getSession()).thenReturn(session);
			when(session.getAttribute("position")).thenReturn(4);
			when(session.getAttribute("email")).thenReturn(EMAIL);
			when(db.userBanned(EMAIL)).thenReturn(0);
			String rs = test.execute(request, response);
			assertEquals(PathContainer.DEFAULT, rs);
		}
	}

}
