package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import classes.Author;
import classes.Book;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class AddBookTest {

	private static final String NAME = "Book_name";

	private static final String SURNAME = "Author_Surname";

	private static final String AUTHOR_NAME = "Author_Name";
	
	private static final Author AUTHOR = Author.createAuthor(AUTHOR_NAME, SURNAME);
	private static final String EMAIL = "email";
	private static final Book BOOK = Book.createBook(1, NAME, AUTHOR, 5);


	private static final AddBook test = new AddBook();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);

	}

	@AfterClass
	public static void redo() {
		
	}

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;

	@Mock
	DBManager db;
	
	@Mock
	Author author;
	
	@Mock
	Book book;
	
	@Captor
	ArgumentCaptor<Author> authorCapture;
	
	@Captor
	ArgumentCaptor<String> bookNameCapture;
	
	@Captor
	ArgumentCaptor<Book> bookCapture;
	
	@Captor
	ArgumentCaptor<Integer> bookIntCapture;
	

	
	@Test
	public void testCorrectOperation() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR_NAME);
		when(request.getParameter("book_author_surname")).thenReturn(SURNAME);
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn(NAME);
		String result = "";
		
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.checkBook(bookCapture.capture())).thenReturn(false);
			doNothing().when(db).addBook(bookCapture.capture());
			when(db.findAuthorId(authorCapture.capture())).thenReturn(0);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
			result = test.execute(request, response);

			String expected = PathContainer.SUCCESS;
			assertEquals(expected, result);

		}
	}
	@Test
	public void testMultipleOperation() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR_NAME);
		when(request.getParameter("book_author_surname")).thenReturn(SURNAME);
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn(NAME);
		when(db.findAuthorId(authorCapture.capture())).thenReturn(1);
		doNothing().when(db).addAuthor(authorCapture.capture());
		when(db.findAuthorByNames(authorCapture.capture())).thenReturn(AUTHOR);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findAuthorByNames(authorCapture.capture())).thenReturn(null);
			doNothing().when(db).addAuthor(authorCapture.capture());
			when(db.checkBook(bookCapture.capture())).thenReturn(true);
			doNothing().when(db).addBook(bookCapture.capture());

			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("We already have this book", thrown.getMessage());
			assertEquals(AUTHOR_NAME, authorCapture.getValue().getName());
			assertEquals(SURNAME, authorCapture.getValue().getSurname());
			assertEquals(NAME, bookCapture.getValue().getName());
			assertEquals(5, bookCapture.getValue().getBookNumber());
		}
	}


	@Test
	public void testNull() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR_NAME);
		when(request.getParameter("book_author_surname")).thenReturn(null);
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn(NAME);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Some information is null", thrown.getMessage());
	}

	@Test
	public void testWrongNumber() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR_NAME);
		when(request.getParameter("book_author_surname")).thenReturn(SURNAME);
		when(request.getParameter("book_number")).thenReturn("sad");
		when(request.getParameter("book_name")).thenReturn(NAME);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Wrong number format", thrown.getMessage());
	}

	@Test
	public void testBlank() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("book_author_name")).thenReturn(AUTHOR_NAME);
		when(request.getParameter("book_author_surname")).thenReturn(" ");
		when(request.getParameter("book_number")).thenReturn("5");
		when(request.getParameter("book_name")).thenReturn(NAME);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("Some information is blank", thrown.getMessage());
	}

	@Test
	public void testPosNull() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You are not loged in", thrown.getMessage());
	}

	@Test
	public void testPosNotCorrect() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You dont have permission for this operation", thrown.getMessage());
	}

}
