package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import classes.Subscription;
import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class GetTicketsTest {

	private static GetTickets test = new GetTickets();
	private static final List<Subscription> TICKET = new ArrayList<>();
	private static final Author AUTHOR = Author.createAuthor(1, "Nmae", "Surname");

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	HttpSession session;
	
	@Mock
	DBManager db;
	@Test
	public void testNullPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(null);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testBlankPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn(" ");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testWrongPage() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("5asd");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("Wrong inputs", thrown.getMessage());
		}
	}
	@Test
	public void testCorrect() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		TICKET.add(Subscription.createSubscription(1,User.createUser(1, "name", "surname", "email", 1, "No"),Book.createBook(2,"Book2", AUTHOR, 5),"APROVED", LocalDateTime.now(), 5));
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("2");
			when(db.findLimitTickets(6, 6)).thenReturn(TICKET);
			String res = test.execute(request, response);
			verify(request).setAttribute(eq("numberOfPages"), eq(3));
			verify(request).setAttribute(eq("currentPage"), eq(2));
			verify(request).setAttribute(eq("list_subs"), eq(TICKET));
			assertEquals(PathContainer.LIBRARIAN_TICKETS, res);
			
		}
	}
	@Test
	public void testWrongPos() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(1);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("2");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}
	@Test
	public void testNullPos() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findNumberOfTickets()).thenReturn(18);
			when(request.getParameter("page")).thenReturn("2");
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.execute(request, response);
			});
			assertEquals("You don`t have a permission to enter here", thrown.getMessage());
		}
	}
}
