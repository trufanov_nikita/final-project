package command;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.User;
import controller.PathContainer;
import database.DBManager;
import exception.WebException;

public class BanTest {
	private static Ban test = new Ban();
	final static String EMAIL = "email";
	final static String NAME = "name";
	final static String SURNAME = "surname";
	final static int POS = 2;
	
	final static User USER = User.createUser(NAME, SURNAME, EMAIL, POS);
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	DBManager db;

	@Mock
	HttpSession session;
	
	@Captor
	ArgumentCaptor<String> emailCapture;

	@Test
	public void testNullPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}

	@Test
	public void testWrongPos() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(2);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("You don`t have a permission to enter here", thrown.getMessage());
	}

	@Test
	public void testNullInput() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("ban_input")).thenReturn(null);
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("incorrect email(null or blank)", thrown.getMessage());
	}

	@Test
	public void testBlankInput() {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("ban_input")).thenReturn(" ");
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("incorrect email(null or blank)", thrown.getMessage());
	}

	@Test
	public void testWrongInput() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("ban_input")).thenReturn(EMAIL);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(null);
			
			doNothing().when(db).banUser(emailCapture.capture());
			 WebException thrown = Assert.assertThrows(WebException.class, () -> {
				 test.execute(request, response);
			 });
			 assertEquals("No such user", thrown.getMessage());
			assertEquals(EMAIL, emailCapture.getValue());
			
		}
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("No such user", thrown.getMessage());
	}
	@Test
	public void testCorrectInput() throws WebException {
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("position")).thenReturn(3);
		when(request.getParameter("ban_input")).thenReturn(EMAIL);
		try (MockedStatic<DBManager> dbm = Mockito.mockStatic(DBManager.class)) {
			dbm.when(DBManager::createDb).thenReturn(db);
			when(db.findUser(emailCapture.capture())).thenReturn(USER);
			
			doNothing().when(db).banUser(emailCapture.capture());
			String res = test.execute(request, response);
			assertEquals(PathContainer.SUCCESS, res);
			List<String> list = emailCapture.getAllValues();
			assertEquals(EMAIL, list.get(0));
			assertEquals( EMAIL, list.get(1));
		}
		WebException thrown = Assert.assertThrows(WebException.class, () -> {
			test.execute(request, response);
		});
		assertEquals("No such user", thrown.getMessage());
	}

}
