package database;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import classes.Author;
import classes.Book;
import classes.User;
import exception.WebException;

public class DBManagerTest {
	private static DBManager test = new DBManager();
	private static final String CONNECTION_URL = "jdbc:mysql://127.0.0.1:3306/mydb?user=root&password=baluha";
	private static final String EXCEPION = "Error with operation. Your request will rollback";
	private static final User USER_ONE = User.createUser(1, "name", "surname", "email", 1, "No");
	private static final User USER_TWO = User.createUser(2, "name2", "surname2", "email2", 1, "Yes");
	
	private static final Author AUTHOR = Author.createAuthor(1, "name", "surname");
	private static final Book BOOK_ONE = Book.createBook(1, "name", AUTHOR, 5);
	private static final Book BOOK_TWO = Book.createBook(2, "name", AUTHOR, 5);
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	@Mock
	DBManager db;
	
	@Mock
	Connection con;
	@Mock
	PreparedStatement stmt;
	@Mock
	ResultSet rs;
	@Captor
	ArgumentCaptor<String> stringCapture;

	@Test
	public void testIsEmailBusyFalse() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			doNothing().when(con).commit();
			boolean res = test.isEmailBusy("email");
			assertEquals(false, res);
			assertEquals("Select * from users where email = ?", stringCapture.getValue());
		}
	}
	@Test
	public void testIsEmailBusyTrue() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true);
			doNothing().when(con).commit();
			boolean res = test.isEmailBusy("email");
			assertEquals(true, res);
			assertEquals("Select * from users where email = ?", stringCapture.getValue());
		}
	}
	@Test
	public void testIsEmailBusyException() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			when(rs.next()).thenReturn(true);
			doNothing().when(con).commit();
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.isEmailBusy("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
			assertEquals("Select * from users where email = ?", stringCapture.getValue());
		}
	}
	@Test
	public void testIsPassCorrectTrue() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			doNothing().when(con).commit();
			boolean res = test.isPassCorrect("email", "pass");
			assertEquals(true, res);
			assertEquals("Select * from users where password=? and email=?;", stringCapture.getValue());
		}
	}
	@Test
	public void testIsPassCorrectFalse() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			doNothing().when(con).commit();
			boolean res = test.isPassCorrect("email", "pass");
			assertEquals(false, res);
			assertEquals("Select * from users where password=? and email=?;", stringCapture.getValue());
		}
	}
	@Test
	public void testIsPassCorrectException() throws WebException, SQLException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			when(rs.next()).thenReturn(false);
			doNothing().when(con).commit();
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.isPassCorrect("email", "pass");
			});
			assertEquals(EXCEPION, thrown.getMessage());
			assertEquals("Select * from users where password=? and email=?;", stringCapture.getValue());
		}
	}
	@Test
	public void testAddAuthor() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			doNothing().when(con).commit();
			test.addAuthor(AUTHOR);
			verify(stmt, times(1)).execute();
			
		}
	}
	@Test
	public void testAddAuthorException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			doNothing().when(con).commit();
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.addAuthor(AUTHOR);
			});
			assertEquals(EXCEPION, thrown.getMessage());
			
		}
	}
	@Test
	public void testAddUser() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			doNothing().when(con).commit();
			test.addUser("name", "surname", "email", "password");
			verify(stmt, times(1)).execute();
			
		}
	}
	@Test
	public void testAddUserException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			doNothing().when(con).commit();
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.addUser("name", "surname", "email", "password");
			});
			assertEquals(EXCEPION, thrown.getMessage());
			
		}
	}
	@Test
	public void testFindAuthorId() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(1).thenReturn(2);
			int res = test.findAuthorId(AUTHOR);
			assertEquals(1, res);
		}
	}
	@Test
	public void testFindAuthorIdException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findAuthorId(AUTHOR);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindAuthorIdNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			int res = test.findAuthorId(AUTHOR);
			assertEquals(0, res);
		}
	}
	@Test
	public void testFindAuthorsBooks() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(1).thenReturn(2);
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(5)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			List<Book> res = test.findAuthorsBooks(AUTHOR);
			List<Book> expected = new ArrayList<Book>();
			expected.add(BOOK_ONE);
			expected.add(BOOK_TWO);
			assertEquals(expected, res);
		}
	}
	@Test
	public void testFindAuthorsBooksException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findAuthorsBooks(AUTHOR);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindAuthorById() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(AUTHOR.getId()).thenReturn(22);
			when(rs.getString(2)).thenReturn(AUTHOR.getName()).thenReturn("wrong name");
			when(rs.getString(3)).thenReturn(AUTHOR.getSurname()).thenReturn("wrong surname");
			Author res = test.findAuthorById(1);
			assertEquals(AUTHOR, res);
			
		}
	}
	@Test
	public void testFindAuthorByIdException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findAuthorById(1);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindAuthorByIdNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			Author res = test.findAuthorById(1);
			assertNull(res);
		}
	}
	@Test
	public void testFindAuthorByNames() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(AUTHOR.getId()).thenReturn(22);
			when(rs.getString(2)).thenReturn(AUTHOR.getName()).thenReturn("wrong name");
			when(rs.getString(3)).thenReturn(AUTHOR.getSurname()).thenReturn("wrong surname");
			Author res = test.findAuthorByNames(AUTHOR);
			assertEquals(AUTHOR, res);
			
		}
	}
	@Test
	public void testFindAuthorByNamesNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			Author res = test.findAuthorByNames(AUTHOR);
			assertNull(res);
			
		}
	}
	@Test
	public void testFindAuthorByNamesException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findAuthorByNames(AUTHOR);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindUser() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_ONE.getId()).thenReturn(USER_TWO.getId());
			when(rs.getString(2)).thenReturn(USER_ONE.getName()).thenReturn(USER_TWO.getName());
			when(rs.getString(3)).thenReturn(USER_ONE.getSurname()).thenReturn(USER_TWO.getSurname());
			when(rs.getString(4)).thenReturn(USER_ONE.getEmail()).thenReturn(USER_TWO.getEmail());
			when(rs.getInt(6)).thenReturn(USER_ONE.getPosition()).thenReturn(USER_TWO.getPosition());
			when(rs.getInt(7)).thenReturn(0).thenReturn(1);

			User res = test.findUser("email");
			assertEquals(USER_ONE, res);
			
		}
	}
	@Test
	public void testFindUserOther() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_TWO.getId()).thenReturn(USER_ONE.getId());
			when(rs.getString(2)).thenReturn(USER_TWO.getName()).thenReturn(USER_ONE.getName());
			when(rs.getString(3)).thenReturn(USER_TWO.getSurname()).thenReturn(USER_ONE.getSurname());
			when(rs.getString(4)).thenReturn(USER_TWO.getEmail()).thenReturn(USER_ONE.getEmail());
			when(rs.getInt(6)).thenReturn(USER_TWO.getPosition()).thenReturn(USER_ONE.getPosition());
			when(rs.getInt(7)).thenReturn(1).thenReturn(0);

			User res = test.findUser("email");
			assertEquals(USER_TWO, res);
			
		}
	}
	@Test
	public void testFindUserNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			User res = test.findUser("email");
			assertNull(res);
			
		}
	}
	@Test
	public void testFindUserException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findUser("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindUserById() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_ONE.getId()).thenReturn(USER_TWO.getId());
			when(rs.getString(2)).thenReturn(USER_ONE.getName()).thenReturn(USER_TWO.getName());
			when(rs.getString(3)).thenReturn(USER_ONE.getSurname()).thenReturn(USER_TWO.getSurname());
			when(rs.getString(4)).thenReturn(USER_ONE.getEmail()).thenReturn(USER_TWO.getEmail());
			when(rs.getInt(6)).thenReturn(USER_ONE.getPosition()).thenReturn(USER_TWO.getPosition());
			when(rs.getInt(7)).thenReturn(0).thenReturn(1);

			User res = test.findUserById(1);
			assertEquals(USER_ONE, res);
			
		}
	}
	@Test
	public void testFindUserByIdOther() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_TWO.getId()).thenReturn(USER_ONE.getId());
			when(rs.getString(2)).thenReturn(USER_TWO.getName()).thenReturn(USER_ONE.getName());
			when(rs.getString(3)).thenReturn(USER_TWO.getSurname()).thenReturn(USER_ONE.getSurname());
			when(rs.getString(4)).thenReturn(USER_TWO.getEmail()).thenReturn(USER_ONE.getEmail());
			when(rs.getInt(6)).thenReturn(USER_TWO.getPosition()).thenReturn(USER_ONE.getPosition());
			when(rs.getInt(7)).thenReturn(1).thenReturn(0);

			User res = test.findUserById(1);
			assertEquals(USER_TWO, res);
			
		}
	}
	@Test
	public void testFindUserByIdNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			User res = test.findUserById(1);
			assertNull(res);
			
		}
	}
	@Test
	public void testFindUserByIdException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findUserById(1);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindNumberOfBooks() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(22);
			int res = test.findNumberOfBooks();
			assertEquals(22, res);
		}
	}
	@Test
	public void testFindNumberOfBooksNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			int res = test.findNumberOfBooks();
			assertEquals(0, res);
		}
	}
	@Test
	public void testFindNumberOfBooksException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findNumberOfBooks();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindNumberOfUsers() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(22);
			int res = test.findNumberOfUsers();
			assertEquals(22, res);
		}
	}
	@Test
	public void testFindNumberOfUsersNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			int res = test.findNumberOfUsers();
			assertEquals(0, res);
		}
	}
	@Test
	public void testFindNumberOfUsersException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findNumberOfUsers();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindNumberOfTickets() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(22);
			int res = test.findNumberOfTickets();
			assertEquals(22, res);
		}
	}
	@Test
	public void testFindNumberOfTicketsNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			int res = test.findNumberOfTickets();
			assertEquals(0, res);
		}
	}
	@Test
	public void testFindNumberOfTicketsException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findNumberOfTickets();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindNumberOfAuthors() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(22);
			int res = test.findNumberOfAuthors();
			assertEquals(22, res);
		}
	}
	@Test
	public void testFindNumberOfAuthorsNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			int res = test.findNumberOfAuthors();
			assertEquals(0, res);
		}
	}
	@Test
	public void testFindNumberOfAuthorsException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findNumberOfAuthors();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindLimitBooks() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(4)).thenReturn(AUTHOR.getId()).thenReturn(AUTHOR.getId());
			when(rs.getInt(1)).thenReturn(BOOK_ONE.getId()).thenReturn(BOOK_TWO.getId());
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(3)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			when(rs.getString(5)).thenReturn(AUTHOR.getName()).thenReturn(AUTHOR.getName());
			when(rs.getString(6)).thenReturn(AUTHOR.getSurname()).thenReturn(AUTHOR.getSurname());
			
			List<Book> res = test.findLimitBooks(0, 10);
			assertEquals(books, res);
		}
	}
	@Test
	public void testFindLimitBooksNotFound() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			List<Book> res = test.findLimitBooks(0, 10);
			assertTrue(res.isEmpty());
		}
	}
	@Test
	public void testFindLimitBooksException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findLimitBooks(0,10);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindLimitUsers() throws SQLException, WebException {
		List<User> users = new ArrayList<>();
		users.add(USER_ONE);
		users.add(USER_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_ONE.getId()).thenReturn(USER_TWO.getId());
			when(rs.getString(2)).thenReturn(USER_ONE.getName()).thenReturn(USER_TWO.getName());
			when(rs.getString(3)).thenReturn(USER_ONE.getSurname()).thenReturn(USER_TWO.getSurname());
			when(rs.getString(4)).thenReturn(USER_ONE.getEmail()).thenReturn(USER_TWO.getEmail());
			when(rs.getInt(6)).thenReturn(USER_ONE.getPosition()).thenReturn(USER_TWO.getPosition());
			when(rs.getInt(7)).thenReturn(0).thenReturn(1);

			List<User> res = test.findLimitUsers(0, 10);
			assertEquals(users, res);
			
		}
	}
	@Test
	public void testFindLimitUsersOther() throws SQLException, WebException {
		List<User> users = new ArrayList<>();
		users.add(USER_TWO);
		users.add(USER_ONE);
		
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(USER_TWO.getId()).thenReturn(USER_ONE.getId());
			when(rs.getString(2)).thenReturn(USER_TWO.getName()).thenReturn(USER_ONE.getName());
			when(rs.getString(3)).thenReturn(USER_TWO.getSurname()).thenReturn(USER_ONE.getSurname());
			when(rs.getString(4)).thenReturn(USER_TWO.getEmail()).thenReturn(USER_ONE.getEmail());
			when(rs.getInt(6)).thenReturn(USER_TWO.getPosition()).thenReturn(USER_ONE.getPosition());
			when(rs.getInt(7)).thenReturn(1).thenReturn(0);

			List<User> res = test.findLimitUsers(0, 10);
			assertEquals(users, res);
			
		}
	}
	@Test
	public void testFindLimitUsersNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			List<User> res = test.findLimitUsers(0, 10);
			assertTrue(res.isEmpty());
			
		}
	}
	@Test
	public void testFindLimitUsersException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findLimitUsers(0, 10);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindSearchBooks() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(5)).thenReturn(AUTHOR.getId()).thenReturn(AUTHOR.getId());
			when(rs.getInt(1)).thenReturn(BOOK_ONE.getId()).thenReturn(BOOK_TWO.getId());
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(3)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			when(rs.getString(6)).thenReturn(AUTHOR.getName()).thenReturn(AUTHOR.getName());
			when(rs.getString(7)).thenReturn(AUTHOR.getSurname()).thenReturn(AUTHOR.getSurname());
			
			List<Book> res = test.findSearchBook("search");
			assertEquals(books, res);
		}
	}
	@Test
	public void testFindSearchBookNotFound() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			List<Book> res = test.findSearchBook("search");
			assertTrue(res.isEmpty());
		}
	}
	@Test
	public void testFindSearchBookException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findSearchBook("search");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindMostPopularBooks() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(5)).thenReturn(AUTHOR.getId()).thenReturn(AUTHOR.getId());
			when(rs.getInt(1)).thenReturn(BOOK_ONE.getId()).thenReturn(BOOK_TWO.getId());
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(3)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			when(rs.getString(6)).thenReturn(AUTHOR.getName()).thenReturn(AUTHOR.getName());
			when(rs.getString(7)).thenReturn(AUTHOR.getSurname()).thenReturn(AUTHOR.getSurname());
			
			List<Book> res = test.findMostPopularBooks();
			assertEquals(books, res);
		}
	}
	@Test
	public void testFindMostPopularBooksNotFound() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			List<Book> res = test.findMostPopularBooks();
			assertTrue(res.isEmpty());
		}
	}
	@Test
	public void testFindMostPopularBooksException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findMostPopularBooks();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindBookById() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(4)).thenReturn(AUTHOR.getId()).thenReturn(AUTHOR.getId());
			when(rs.getInt(1)).thenReturn(BOOK_ONE.getId()).thenReturn(BOOK_TWO.getId());
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(3)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			when(rs.getString(5)).thenReturn(AUTHOR.getName()).thenReturn(AUTHOR.getName());
			when(rs.getString(6)).thenReturn(AUTHOR.getSurname()).thenReturn(AUTHOR.getSurname());
			
			Book res = test.findBookById(1);
			assertEquals(BOOK_ONE, res);
		}
	}
	@Test
	public void testFindBookByIdNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			Book res = test.findBookById(1);
			assertNull(res);
		}
	}
	@Test
	public void testFindBookByIdException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findBookById(1);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testFindBooksWithAuthors() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(4)).thenReturn(AUTHOR.getId()).thenReturn(AUTHOR.getId());
			when(rs.getInt(1)).thenReturn(BOOK_ONE.getId()).thenReturn(BOOK_TWO.getId());
			when(rs.getString(2)).thenReturn(BOOK_ONE.getName()).thenReturn(BOOK_TWO.getName());
			when(rs.getInt(3)).thenReturn(BOOK_ONE.getBookNumber()).thenReturn(BOOK_TWO.getBookNumber());
			when(rs.getString(5)).thenReturn(AUTHOR.getName()).thenReturn(AUTHOR.getName());
			when(rs.getString(6)).thenReturn(AUTHOR.getSurname()).thenReturn(AUTHOR.getSurname());
			
			List<Book> res = test.findBooksWithAuthors();
			assertEquals(books, res);
		}
	}
	@Test
	public void testFindBooksWithAuthorsNotFound() throws SQLException, WebException {
		List<Book> books = new ArrayList<>();
		books.add(BOOK_ONE);
		books.add(BOOK_TWO);
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			List<Book> res = test.findBooksWithAuthors();
			assertTrue(res.isEmpty());
		}
	}
	@Test
	public void testFindBooksWithAuthorsException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.findBooksWithAuthors();
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testUserBanned() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(1).thenReturn(0);
			int res = test.userBanned("email");
			assertEquals(1, res);
		}
	}
	@Test
	public void testtestUserBannedNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			
			int res = test.userBanned("email");
			assertEquals(0, res);
		}
	}
	@Test
	public void testUserBannedException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.userBanned("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testBanUser() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.banUser("email");
			verify(stmt, times(1)).execute();
		}
	}
	@Test
	public void testBanUserException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.banUser("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testUnBanUser() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.unbanUser("email");
			verify(stmt, times(1)).execute();
		}
	}
	@Test
	public void testUnBanUserException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.unbanUser("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testAddBook() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.addBook(BOOK_ONE);
			verify(stmt, times(1)).execute();
		}
	}
	@Test
	public void testAddBookException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.addBook(BOOK_ONE);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testCheckBookTrue() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(1);
			boolean res = test.checkBook(BOOK_ONE);
			assertEquals(true, res);
		}
	}
	@Test
	public void testCheckBookFalse() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(true).thenReturn(false);
			when(rs.getInt(1)).thenReturn(0);
			boolean res = test.checkBook(BOOK_ONE);
			assertEquals(false, res);
		}
	}
	@Test
	public void testCheckBookNotFound() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenReturn(rs);
			when(rs.next()).thenReturn(false);
			when(rs.getInt(1)).thenReturn(0);
			boolean res = test.checkBook(BOOK_ONE);
			assertEquals(false, res);
		}
	}
	@Test
	public void testCheckBookException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.executeQuery()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.checkBook(BOOK_ONE);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testEditBook() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.editBook(BOOK_ONE);
			verify(stmt, times(1)).execute();
		}
	}
	@Test
	public void testEditBookException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.editBook(BOOK_ONE);
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testCreateLibrarianException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.createLibrarian("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testCreateLibrarian() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.createLibrarian("email");
			verify(stmt, times(1)).execute();
		}
	}
	@Test
	public void testDeleteLibrarianException() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenThrow(SQLException.class);
			WebException thrown = Assert.assertThrows(WebException.class, () -> {
				test.deleteLibrarian("email");
			});
			assertEquals(EXCEPION, thrown.getMessage());
		}
	}
	@Test
	public void testDeleteLibrarian() throws SQLException, WebException {
		try (MockedStatic<DriverManager> dm = Mockito.mockStatic(DriverManager.class)) {
			dm.when(() ->DriverManager.getConnection(CONNECTION_URL)).thenReturn(con);
			when(con.prepareStatement(stringCapture.capture())).thenReturn(stmt);
			when(stmt.execute()).thenReturn(true);
			test.deleteLibrarian("email");
			verify(stmt, times(1)).execute();
		}
	}
	
	
	
}

